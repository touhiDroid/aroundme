package com.buet.aroundme.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.buet.aroundme.model.ServerResponse;
import com.buet.aroundme.util.AppConstants;

public class JsonParser {

	private static InputStream is = null;
	private static JSONObject jObj = null;
	private static String json = "";

	private static final String TAG = JsonParser.class.getSimpleName();

	// constructor
	public JsonParser() {

	}

	// public ServerResponse retrieveServerData(Context context,
	// List<NameValuePair> urlParams) throws ConnectTimeoutException {
	// String appToken = "";//AppPrefManager.getXToken(context);
	// return retrieveServerData(AppConstants.REQUEST_TYPE_POST,
	// AppConstants.API_URL, urlParams, null, appToken);
	//
	// }

	public ServerResponse retrieveServerData(String jsonContent)
			throws ConnectTimeoutException {
		String appToken = "";//AppPrefManager.getXToken(context);
		return retrieveServerData(AppConstants.REQUEST_TYPE_POST,
				AppConstants.API_URL, null, jsonContent, appToken);

	}

	public ServerResponse retrieveServerData(int reqType, String url,
			List<NameValuePair> urlParams, String content, String appToken)
			throws ConnectTimeoutException {
		Log.d(TAG, "in retrieveServerData method");

		int status = 0;

		StringBuilder sb = null;
		if (urlParams != null) {
			String paramString = URLEncodedUtils.format(urlParams, "utf-8");
			url += "?" + paramString;
		}
		Log.d(TAG, "url after param added = " + url);
		Log.d(TAG, "content body = " + content);

		// Making HTTP request
		try {
			HttpParams httpParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParams,
					AppConstants.TIMEOUT_NET_CONNECTION);
			HttpConnectionParams.setSoTimeout(httpParams,
					AppConstants.TIMEOUT_SOCKET_CONNECTION);
			DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
			HttpResponse httpResponse = null;

			if (reqType == AppConstants.REQUEST_TYPE_GET) {
				HttpGet httpGet = new HttpGet(url);
				// httpGet.setHeader("Content-Type", "application/json");
				// httpGet.setHeader("Accept", "application/json");
				if (appToken != null) {
					httpGet.setHeader("x-token", appToken);
				}

				httpResponse = httpClient.execute(httpGet);

			} else if (reqType == AppConstants.REQUEST_TYPE_POST) {
				HttpPost httpPost = new HttpPost(url);
				// httpPost.setHeader("Content-Type", "application/json");
				// httpPost.setHeader("Accept", "application/json");
				if (appToken != null) {
					httpPost.setHeader("x-token", appToken);
				}

				if (content != null) {
					StringEntity se = new StringEntity(content);
					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
							"application/json"));
					httpPost.setEntity(se);
				}

				httpResponse = httpClient.execute(httpPost);
			} else if (reqType == AppConstants.REQUEST_TYPE_PUT) {
				HttpPut httpPut = new HttpPut(url);
				// httpPut.setHeader("Content-Type", "application/json");
				// httpPut.setHeader("Accept", "application/json");
				if (appToken != null) {
					httpPut.setHeader("x-token", appToken);
				}

				if (content != null) {
					StringEntity se = new StringEntity(content);
					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
							"application/json"));
					httpPut.setEntity(se);
				}

				httpResponse = httpClient.execute(httpPut);
			} else if (reqType == AppConstants.REQUEST_TYPE_DELETE) {
				HttpDelete httpDelete = new HttpDelete(url);
				// httpDelete.setHeader("Content-Type", "application/json");
				// httpDelete.setHeader("Accept", "application/json");
				if (appToken != null) {
					httpDelete.setHeader("x-token", appToken);
				}

				if (content != null) {
					StringEntity se = new StringEntity(content);
					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
							"application/json"));
					((HttpResponse) httpDelete).setEntity(se);
				}

				httpResponse = httpClient.execute(httpDelete);
			}

			status = httpResponse.getStatusLine().getStatusCode();
			Log.d(TAG, "STAUS = " + status);

			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (ConnectTimeoutException cte) {
			throw cte;
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			Log.d(TAG, "trying to read input stream.");
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			Log.d(TAG, "sb = " + sb.toString());
			json = sb.toString();
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}

		// try parse the string to a JSON object
		try {
			jObj = new JSONObject(json);
		} catch (JSONException e) {
			Log.e("JSON Parser", "Error parsing data " + e.toString());
		}

		// return ServerResponse
		return new ServerResponse(jObj, status);
	}

	public ServerResponse postAndGetServerData(String url,
			ArrayList<NameValuePair> listedFormData) {
		Log.d(TAG, "in retrieveServerData method");

		int status = 0;

		StringBuilder sb = null;

		Log.d(TAG, "url = " + url);
		Log.d(TAG, "listedFormData = " + listedFormData.toString());

		// Making HTTP POST request
		try {
			HttpParams httpParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParams,
					AppConstants.TIMEOUT_NET_CONNECTION);
			HttpConnectionParams.setSoTimeout(httpParams,
					AppConstants.TIMEOUT_SOCKET_CONNECTION);
			DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
			HttpResponse httpResponse = null;
			HttpPost httpPost = new HttpPost(url);

			if (listedFormData != null) {
				httpPost.setEntity(new UrlEncodedFormEntity(listedFormData));
			}
			httpResponse = httpClient.execute(httpPost);

			status = httpResponse.getStatusLine().getStatusCode();
			Log.d(TAG, "STAUS = " + status);

			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			Log.d(TAG, "trying to read input stream.");
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(is));
			sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				Log.i(TAG, "line: " + line);
				sb.append(line + "\n");
			}
			is.close();
			Log.d(TAG, "sb = " + sb.toString());
			json = sb.toString();
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}

		// try parse the string to a JSON object
		try {
			jObj = new JSONObject(json);
		} catch (JSONException e) {
			Log.e(TAG, "Error parsing data " + e.toString());
		}

		// return ServerResponse
		return new ServerResponse(jObj, status);
	}
}
