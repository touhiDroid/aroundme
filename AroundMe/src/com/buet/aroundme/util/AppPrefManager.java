/**
 * 
 */
package com.buet.aroundme.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.buet.aroundme.model.User;

/**
 * @author Touhid
 * 
 */
public class AppPrefManager {

	// get a value to check the nullity of the pref. data
	private static SharedPreferences prefs;
	private static SharedPreferences.Editor prefsEditor;

	public static final String DEF_NAME = "<Set Your Name>";
	public static final String DEF_EMAIL = "<Set An Email>";
	public static final String DEF_ADDRESS = "<Set Your Address>";

	@SuppressWarnings("unused")
	private static final String TAG = "AppPrefManager";

	public AppPrefManager(Context context) {
		if (prefs == null)
			prefs = PreferenceManager.getDefaultSharedPreferences(context);
		if (prefsEditor == null)
			prefsEditor = PreferenceManager
					.getDefaultSharedPreferences(context).edit();
	}

	public static void setConfigsOn(Context context) {
		if (prefs == null)
			prefs = PreferenceManager.getDefaultSharedPreferences(context);
		if (prefsEditor == null)
			prefsEditor = PreferenceManager
					.getDefaultSharedPreferences(context).edit();
	}

	private static void assurePrefNotNull(Context context) {
		if (prefs == null)
			prefs = PreferenceManager.getDefaultSharedPreferences(context);
		if (prefsEditor == null)
			prefsEditor = PreferenceManager
					.getDefaultSharedPreferences(context).edit();
	}

	public static void setUserData(Context context, User user) {
		assurePrefNotNull(context);
		if (prefs == null)
			prefs = PreferenceManager.getDefaultSharedPreferences(context);
		if (prefsEditor == null)
			prefsEditor = PreferenceManager
					.getDefaultSharedPreferences(context).edit();
		prefsEditor.putInt(AppConstants.PREF_ID, user.getId());
		prefsEditor.putString(AppConstants.PREF_NAME, user.getName());
		prefsEditor.putString(AppConstants.PREF_PASSWORD, user.getPassword());
		prefsEditor.putString(AppConstants.PREF_EMAIL, user.getEmail());
		prefsEditor.putString(AppConstants.PREF_DOB, user.getDob());
		prefsEditor.putString(AppConstants.PREF_ADDRESS, user.getAddress());
		prefsEditor.putString(AppConstants.PREF_CONTACT_NO,
				user.getContact_no());
		prefsEditor.putInt(AppConstants.PREF_POINTS, user.getPoints());
		prefsEditor.putString(AppConstants.PREF_PIC_URL, user.getPic_url());
		prefsEditor.putString(AppConstants.PREF_XTOKEN, user.getXtoken());
		prefsEditor.putString(AppConstants.PREF_LAST_IMEI, user.getLast_imei());
		prefsEditor.putString(AppConstants.PREF_LAST_IP, user.getLast_ip());
		prefsEditor.putString(AppConstants.PREF_LAST_LOGIN,
				user.getLast_login());
		prefsEditor.putString(AppConstants.PREF_LAST_LONGI,
				user.getLast_longi() + "");
		prefsEditor.putString(AppConstants.PREF_LAST_LATTI,
				user.getLast_latti() + "");
		prefsEditor.putString(AppConstants.PREF_UPDATED_AT,
				user.getUpdated_at());
		prefsEditor.putString(AppConstants.PREF_CREATED_AT,
				user.getCreated_at());
		prefsEditor.putInt(AppConstants.PREF_GENDER, user.getGender());
		prefsEditor.putString(AppConstants.PREF_GCM_ID, user.getGcm_id());
		prefsEditor.putString(AppConstants.PREF_LAST_MAC, user.getLast_mac());

		prefsEditor.commit();
	}

	public static boolean isUserLoggedIn(Context context) {
		assurePrefNotNull(context);
		String imei = prefs.getString(AppConstants.PREF_LAST_IMEI, "");
		return !(imei.equals("") || imei.length() == 0);
		// return false;
		// else
		// return true;
	}

	public static boolean matchPassword(Context context, String curPassword) {
		assurePrefNotNull(context);
		String pwd = prefs.getString(AppConstants.PREF_PASSWORD, "");
		return pwd.equals(curPassword);
	}

	public static boolean isUSerProfileComplete(Context context) {
		assurePrefNotNull(context);
		int id = prefs.getInt(AppConstants.PREF_ID, -1);
		String name = prefs.getString(AppConstants.PREF_PASSWORD, "");
		String email = prefs.getString(AppConstants.PREF_EMAIL, "");
		String address = prefs.getString(AppConstants.PREF_ADDRESS, "");
		String contactNo = prefs.getString(AppConstants.PREF_CONTACT_NO, "");
		String password = prefs.getString(AppConstants.PREF_PASSWORD, "");
		return id > -1
				&& !(name.equals(null) || name.equals("") || email.equals(null)
						|| email.equals("") || address.equals(null)
						|| address.equals("") || contactNo.equals(null)
						|| contactNo.equals("") || password.equals(null) || password
							.equals(""));
	}

	public static int getUserID(Context thisContext) {
		assurePrefNotNull(thisContext);
		return prefs.getInt(AppConstants.PREF_ID, -1);
	}

	public static String getUserName(Context thisContext) {
		assurePrefNotNull(thisContext);
		return prefs.getString(AppConstants.PREF_NAME, "");
	}

	public static String getUserEmail(Context thisContext) {
		assurePrefNotNull(thisContext);
		return prefs.getString(AppConstants.PREF_EMAIL, "");
	}

	public static String getUserContactNo(Context thisContext) {
		assurePrefNotNull(thisContext);
		return prefs.getString(AppConstants.PREF_CONTACT_NO, "");
	}

	public static String getUserDob(Context thisContext) {
		assurePrefNotNull(thisContext);
		return prefs.getString(AppConstants.PREF_DOB, "");
	}

	public static String getUserAddress(Context thisContext) {
		assurePrefNotNull(thisContext);
		return prefs.getString(AppConstants.PREF_ADDRESS, "");
	}
}
