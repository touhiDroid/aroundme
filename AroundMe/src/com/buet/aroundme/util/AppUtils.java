/**
 * 
 */
package com.buet.aroundme.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

import org.apache.http.conn.util.InetAddressUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore.MediaColumns;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.buet.aroundme.MainActivity;
import com.buet.aroundme.R;
import com.buet.aroundme.interfaces.RetryCallbackForNoInternet;

/**
 * @author Touhid
 * 
 */
public class AppUtils {

	private static Location location;
	private final static int MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
	private final static int MIN_TIME_BW_UPDATES = 5 * 1000; // 5 sec.

	// private final static int GPS_UPDATE_INTERVAL = 10 * 1000; // 10 sec.

	public static boolean isSDCardMounted() {
		String status = Environment.getExternalStorageState();
		if (status.equals(Environment.MEDIA_MOUNTED))
			return true;
		return false;
	}

	public static boolean isInternetAvailable(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	/**
	 * Get IP address from first non-localhost interface
	 * 
	 * @param ipv4
	 *            true=return ipv4, false=return ipv6
	 * @return address or empty string
	 */
	@SuppressLint("DefaultLocale")
	public static String getDeviceIp(boolean useIPv4) {
		try {
			List<NetworkInterface> interfaces = Collections
					.list(NetworkInterface.getNetworkInterfaces());
			for (NetworkInterface intf : interfaces) {
				List<InetAddress> addrs = Collections.list(intf
						.getInetAddresses());
				for (InetAddress addr : addrs) {
					if (!addr.isLoopbackAddress()) {
						String sAddr = addr.getHostAddress().toUpperCase();
						boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
						if (useIPv4) {
							if (isIPv4)
								return sAddr;
						} else {
							if (!isIPv4) {
								// drop ip6 port suffix
								int delim = sAddr.indexOf('%');
								return delim < 0 ? sAddr : sAddr.substring(0,
										delim);
							}
						}
					}
				}
			}
		} catch (Exception ex) {
		} // for now eat exceptions
		return "";
	}


	public static Bitmap getRoundedImage(Bitmap bitmap, int cornerRadius) {
		Bitmap scaled = Bitmap.createScaledBitmap(bitmap, cornerRadius,
				cornerRadius, false);
		return RoundedDrawable.fromBitmap(scaled)
				.setScaleType(ImageView.ScaleType.CENTER_CROP)
				.setCornerRadius(cornerRadius).setOval(true).toBitmap();
	}

	public static String getDeviceMAC(Context context) {
		WifiManager wifiManager = (WifiManager) context
				.getSystemService(Context.WIFI_SERVICE);
		WifiInfo wInfo = wifiManager.getConnectionInfo();
		return wInfo.getMacAddress();
	}

	public static String getDeviceIMEI(Context context) {
		TelephonyManager telephonyManager = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		return telephonyManager.getDeviceId() + "";
	}

	public static int getScreenWidth(Context context) {
		return context.getResources().getDisplayMetrics().widthPixels;
	}

	public static void copyI2OStream(InputStream input, OutputStream output)
			throws IOException {

		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
	}

	public static String getPathFromURI(Context context, Uri uri) {
		String[] projection = { MediaColumns.DATA };
		Cursor cursor = context.getContentResolver().query(uri, projection,
				null, null, null);
		if (cursor != null) {
			// HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
			// THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
			cursor.moveToFirst();
			int columnIndex = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
			String filePath = cursor.getString(columnIndex);
			cursor.close();
			return filePath;
		} else
			return uri.getPath();
	}

	public static int getCorrectionAngleForCam(File picFile) throws IOException {
		ExifInterface exif = new ExifInterface(picFile.getPath());
		int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
				ExifInterface.ORIENTATION_NORMAL);

		int angle = 0;
		if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
			angle = 90;
		} else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
			angle = 180;
		} else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
			angle = 270;
		}
		Log.d("Configurations", "angle = " + angle);
		return angle;
	}

	public static Bitmap decodeFileToBitmap(File f, int imageQuality) {
		try {
			// decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			FileInputStream stream1 = new FileInputStream(f);
			BitmapFactory.decodeStream(stream1, null, o);
			stream1.close();

			// Find the correct scale value. It should be the power of 2.
			final int REQUIRED_SIZE = imageQuality;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;

			while (true) {
				if (width_tmp / 2 < REQUIRED_SIZE
						|| height_tmp / 2 < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}
			Log.i("SCALE", "scale = " + scale);

			// decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			FileInputStream stream2 = new FileInputStream(f);
			Bitmap bitmap = BitmapFactory.decodeStream(stream2, null, o2);
			stream2.close();
			return bitmap;
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void showNoInternetDialog(final Context context,
			final RetryCallbackForNoInternet rc4NoInt, final int retrialNo) {
		final Dialog dialog = new Dialog(context,
				android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
		dialog.setContentView(R.layout.no_internet_dialog);
		dialog.findViewById(R.id.btn_retry_internet_dialog).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.cancel();
						rc4NoInt.retry(retrialNo);
					}
				});
		dialog.findViewById(R.id.btnCloseNoIntDialog).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.cancel();
					}
				});
		dialog.findViewById(R.id.btn_settings_internet_dialog)
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.cancel();
						Intent i = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
						context.startActivity(i);
					}
				});
		Window window = dialog.getWindow();
		window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);

		WindowManager.LayoutParams lp = window.getAttributes();
		lp.dimAmount = 0.7f;
		lp.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		window.setAttributes(lp);
		dialog.show();
	}

	public static void showNoInternetAlert(final Context context) {
		Builder noInternetDialog = new Builder(context);
		noInternetDialog.setTitle("No Active Internet Connection!");
		noInternetDialog.setMessage("You have to have an active internet "
				+ "connection to proceed on. Please turn on a wi-fi "
				+ "connection or enable a data package.");

		noInternetDialog.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						Intent i = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
						context.startActivity(i);
					}
				});
		noInternetDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		noInternetDialog.show();
	}

	public static void showConnectionTimeOutToast(Activity activity) {
		final Context ctx = activity;
		if (Looper.myLooper() != Looper.getMainLooper())
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(
							ctx,
							"Connection to server is lost!\nPlease try again later.",
							Toast.LENGTH_LONG).show();
				}
			});
		else
			Toast.makeText(ctx,
					"Connection to server is lost!\nPlease try again later.",
					Toast.LENGTH_LONG).show();
	}

	public static void showSimpleAlert(Context ctx, String message) {
		AlertDialog.Builder bld = new AlertDialog.Builder(ctx);
		bld.setMessage(message);
		bld.setNeutralButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		bld.create().show();
	}

	public static void toast(final Activity activity, final String msg) {
		if (Looper.getMainLooper() == Looper.myLooper())
			Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
		else
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
				}
			});
	}

	public static void alert(Context context, String message) {
		final AlertDialog.Builder bld = new AlertDialog.Builder(context);
		bld.setMessage(message);
		bld.setNeutralButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		if (Looper.getMainLooper() == Looper.myLooper())
			bld.create().show();
		// else
		// activity.runOnUiThread(new Runnable() {
		// @Override
		// public void run() {
		// bld.create().show();
		// }
		// });
	}

	public static Location getLocation(final Context ctx) {
		final String TAG = "AppUtils:getLocation";
		Log.i(TAG, "getLocation : inside");
		final Handler handler = new Handler();
		// double latitude, longitude;
		handler.post(new Runnable() {

			@Override
			public void run() {
				Log.w(TAG, "getLocation : thread is run");
				LocationManager locationManager = (LocationManager) ctx
						.getSystemService(Context.LOCATION_SERVICE);

				try {

					// getting GPS status
					boolean isGPSEnabled = locationManager
							.isProviderEnabled(LocationManager.GPS_PROVIDER);

					// getting network status
					boolean isNetworkEnabled = locationManager
							.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

					if (!isGPSEnabled && !isNetworkEnabled) {
						// no network provider is enabled
						Log.e(TAG, "No network provider is enabled :\\ ");
					} else {
						// boolean canGetLocation = true;
						// First get location from Network Provider
						if (isNetworkEnabled) {
							locationManager.requestLocationUpdates(
									LocationManager.NETWORK_PROVIDER,
									MIN_TIME_BW_UPDATES,
									MIN_DISTANCE_CHANGE_FOR_UPDATES,
									locListener);
							Log.d("Network", "Network");
							if (locationManager != null) {
								location = locationManager
										.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
								if (location != null) {
									// latitude = location.getLatitude();
									// longitude = location.getLongitude();
								}
							}
						}
						// if GPS Enabled get lat/long using GPS Services
						if (isGPSEnabled) {
							if (location == null) {
								locationManager.requestLocationUpdates(
										LocationManager.GPS_PROVIDER,
										MIN_TIME_BW_UPDATES,
										MIN_DISTANCE_CHANGE_FOR_UPDATES,
										locListener);
								Log.d("GPS Enabled", "GPS Enabled");
								if (locationManager != null) {
									location = locationManager
											.getLastKnownLocation(LocationManager.GPS_PROVIDER);
									if (location != null) {
										// latitude = location.getLatitude();
										// longitude = location.getLongitude();
									}
								}
							}
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				if (location == null)
					location = locationManager
							.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			}
		});
		if (location == null) {
			Log.e(TAG,
					"Location is got null! Returning last updated one from NETWORK_PROVIDER,");
			location = new Location(LocationManager.NETWORK_PROVIDER);
		}
		return location;
	}

	private static LocationListener locListener = new LocationListener() {

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onProviderDisabled(String provider) {
		}

		@Override
		public void onLocationChanged(Location location) {
			AppUtils.location = location;
		}
	};
	
	public static void showProfileCompletionPrompt(final Context tContext) {
		final Dialog dialog = new Dialog(tContext,
				android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
		dialog.setContentView(R.layout.profile_completion_prompt);
		dialog.findViewById(R.id.btnLaterProfilePrompt).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.cancel();
					}
				});
		dialog.findViewById(R.id.btnCloseProfilePrompt).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.cancel();
					}
				});
		dialog.findViewById(R.id.btnUpdateProfilePrompt).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.cancel();
						MainActivity.gotoMyProfile();
					}
				});
		// Center-focus the dialog
		Window window = dialog.getWindow();
		window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);

		// The below code is EXTRA - to dim the parent view by 70% :D
		WindowManager.LayoutParams lp = window.getAttributes();
		lp.dimAmount = 0.7f;
		lp.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		window.setAttributes(lp);
		dialog.show();
	}

	/** Creates a notification with default sound & vibration settings. */
	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	public static void generateNotification(Context context, String message,
			int type, int targetId) {
		int icon = R.drawable.ic_launcher;
		long when = System.currentTimeMillis();
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		// Notification notification = new
		// Notification.Builder(context).setContentTitle("")
		// .setContentText(message)
		// .setSmallIcon(icon).setWhen(when)
		// .build();
		Notification notification = new Notification(icon, message, when);

		String title = context.getString(R.string.app_name);

		Intent notificationIntent = new Intent(context, MainActivity.class);
		if (type == 1) {
			// TODO Set the fragment as News, advertisement or chat
			// target-id as post-id & then retrieve the post there
			// notificationIntent = new Intent(context, SgActivity.class);
			notificationIntent.putExtra("target_id", targetId);
		}
		// TODO Handle 6 other types :: In a very-later phase
		// 2:news, 3:career, 4:university, 5:contest, 6:special, 7:other
		// else if()
		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent intent = PendingIntent.getActivity(context, 0,
				notificationIntent, 0);
		notification.setLatestEventInfo(context, title, message, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		// Play default notification sound
		notification.defaults |= Notification.DEFAULT_SOUND;

		// notification.sound = Uri.parse("android.resource://" +
		// context.getPackageName() + "your_sound_file_name.mp3");

		// Vibrate if vibrate is enabled
		notification.defaults |= Notification.DEFAULT_VIBRATE;
		notificationManager.notify(0, notification);
	}

}
