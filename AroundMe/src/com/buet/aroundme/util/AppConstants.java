/**
 * 
 */
package com.buet.aroundme.util;

import java.io.File;

import android.os.Environment;

/**
 * @author Touhid
 * 
 */
public class AppConstants {

	/** SD Card Folder */
	public static final File APP_DIRECTORY = new File(
			Environment.getExternalStorageDirectory(), "AroundMe");

	public static final String API_URL = "http://6faba4.ngrok.com/AroundMe/Server/aroundme/public/";

	public static final String MINT_API_KEY = "17fcc2a0";

	/** HTTP Request keys */
	public static final int REQUEST_TYPE_GET = 1;
	public static final int REQUEST_TYPE_POST = 2;
	public static final int REQUEST_TYPE_PUT = 3;
	public static final int REQUEST_TYPE_DELETE = 4;

	public static final String SERVER_URL = "";
	public static final String DRAWER_ITEM_NAME = "drawer_item_name";
	public static final String IMAGE_RESOURCE_ID = "drawer_item_image_res";

	public static final int NEWS_UNMODERATED_ACTIVE = 1;
	public static final int NEWS_MODERATED_ACTIVE = 2;
	public static final int NEWS_REPORTED_ACTIVE = 3;
	public static final int NEWS_REPORTED_RESOLVED_ACTIVE = 4;
	public static final int NEWS_UNMODERATED_INACTIVE = 11;
	public static final int NEWS_MODERATED_INACTIVE = 12;
	public static final int NEWS_REPORTED_INACTIVE = 13;
	public static final int NEWS_REPORTED_RESOLVED_INACTIVE = 14;

	/** For count-down formatting */
	public static final int DAY = 24 * 60 * 60;
	public static final int HRS = 60 * 60;
	public static final int MIN = 60;
	/**
	 * Set the timeout in milliseconds until a connection is established. The
	 * default value is zero, that means the timeout is not used.
	 */
	public static final int TIMEOUT_CONNECTION = 60 * 1000;
	public static final int TIMEOUT_NET_CONNECTION = 30 * 1000;
	/**
	 * Set the default socket timeout (SO_TIMEOUT) in milliseconds which is the
	 * timeout for waiting for data.
	 */
	public static final int TIMEOUT_SOCKET = 60 * 1000;
	public static final int TIMEOUT_SOCKET_CONNECTION = 40 * 1000;

	public static final String REQ_STATUS = "status";
	public static final String REQ_DATA = "data";
	public static final String REQ_SUCCESS = "success";
	public static final String REQ_ERROR = "error";

	// News request codes
	public static final String PREF_ID = "_pref_id";
	public static final String PREF_UPDATED_AT = "_pref_updated_at";
	public static final String PREF_CREATED_AT = "_pref_created_at";
	public static final String PREF_NAME = "_pref_name";
	public static final String PREF_PASSWORD = "_pref_pwd";
	public static final String PREF_GENDER = "_pref_gender";
	public static final String PREF_EMAIL = "_pref_email";
	public static final String PREF_DOB = "PREF_DOB";
	public static final String PREF_CONTACT_NO = "PREF_CONTACT_NO";
	public static final String PREF_ADDRESS = "PREF_ADDRESS";
	public static final String PREF_PIC_URL = "PREF_PIC_URL";
	public static final String PREF_POINTS = "PREF_POINTS";
	public static final String PREF_LEVEL = "PREF_LEVEL";
	public static final String PREF_GCM_ID = "PREF_GCM_ID";
	public static final String PREF_XTOKEN = "PREF_XTOKEN";
	public static final String PREF_LAST_IMEI = "PREF_LAST_IMEI";
	public static final String PREF_LAST_IP = "PREF_LAST_IP";
	public static final String PREF_LAST_MAC = "PREF_LAST_MAC";
	public static final String PREF_LAST_LOGIN = "PREF_LAST_LOGIN";
	public static final String PREF_LAST_LONGI = "PREF_LAST_LONGI";
	public static final String PREF_LAST_LATTI = "PREF_LAST_LATTI";

	// User request codes
	public static final int REQ_USER_ACCESS = 1001;
	public static final int REQ_USER_UPDATE_INFO = 1002;
	public static final int REQ_USER_LOG_IN = 1003;
	public static final int REQ_USER_GCM_REGISTER = 1004;
	public static final int REQ_USER_GCM_UNREGISTER = 1005;
	public static final int REQ_USER_UPDATE_LOC = 1006;

	// News request codes
	public static final int REQ_NEWS_POST = 2001;
	public static final int REQ_NEWS_GET_LAT_10_LOC = 2002;
	public static final int REQ_NEWS_GET_LAT_10_LL = 2003;
	public static final int REQ_NEWS_GET_NXT_10_LOC = 2004;
	public static final int REQ_NEWS_GET_NXT_10_LL = 2005;

	public static final String GOOGLE_PROJECT_ID = "621709695259";
	public static final String GCM_DISPLAY_MSG_ACTION = "com.buet.aroundme.gcm.DISPLAY_MESSAGE";

	public static final int REQ_CHAT_POST = 3001;

}
