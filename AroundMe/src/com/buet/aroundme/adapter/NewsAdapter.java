package com.buet.aroundme.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.buet.aroundme.R;
import com.buet.aroundme.model.NewsItem;
import com.buet.aroundme.util.AppConstants;

public class NewsAdapter extends ArrayAdapter<NewsItem> {

	private final String TAG = this.getClass().getSimpleName();

	private Context thisContext;
	private ArrayList<NewsItem> newsList;
	private LayoutInflater mInflater;
	private AQuery aq;
	private Bitmap preset;

	public NewsAdapter(Context context, ArrayList<NewsItem> newsList) {
		super(context, R.layout.news_item, newsList);
		this.newsList = newsList;
		thisContext = context;
		mInflater = (LayoutInflater) thisContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		aq = new AQuery(context);
		preset = BitmapFactory.decodeResource(thisContext.getResources(),
				R.drawable.logo_ph);
	}

	@Override
	public int getCount() {
		return newsList.size();
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	// @SuppressLint("InflateParams")
	@Override
	public View getView(int position, View cv, ViewGroup parent) {
		ViewHolder holder;
		if (cv == null) {
			cv = mInflater.inflate(R.layout.news_item, parent, false);
			holder = new ViewHolder();
			holder.ivNewsImage = (ImageView) cv.findViewById(R.id.ivNewsImage);
			holder.tvHead = (TextView) cv.findViewById(R.id.tvNewsHead);
			holder.tvBody = (TextView) cv.findViewById(R.id.tvNewsBody);
			holder.tvUpTime = (TextView) cv.findViewById(R.id.tvNewsUpTime);
		} else
			holder = (ViewHolder) cv.getTag();

		NewsItem item = (NewsItem) getItem(position);
		try {
			String h = item.getHead();
			holder.tvHead.setText(h);
			String b = item.getBody();
			holder.tvBody.setText(b);
			holder.tvUpTime.setText(item.getUpdatedAt());
			Log.d(TAG, "News adapter getView() :: News Head=" + item.getHead());
			String iUrl = item.getImageUrls();
			// TODO set image according to settings image-loading preference
			if (!iUrl.equals(null) && !iUrl.equals("") && iUrl.length() > 0) {
				aq = new AQuery(cv);
				iUrl = AppConstants.API_URL + iUrl;
				aq.id(holder.ivNewsImage).image(iUrl, true, true, 0,
						R.drawable.logo_ph, preset, AQuery.FADE_IN,
						AQuery.RATIO_PRESERVE);
			} else
				holder.ivNewsImage.setVisibility(View.GONE);
		} catch (Exception e) {
			Log.e(TAG, "Exception setting news item: " + item.getHead() + ",\n"
					+ e.toString());
			e.printStackTrace();
		}

		return cv;
	}

	private class ViewHolder {
		ImageView ivNewsImage;
		TextView tvHead, tvBody, tvUpTime;
	}

	public void setData(ArrayList<NewsItem> newsList) {
		clear();
		if (newsList != null) {
			this.newsList = newsList;
			for (int i = 0; i < newsList.size(); i++) {
				add(newsList.get(i));
			}
		}
	}

	public void addData(ArrayList<NewsItem> newsListToAdd) {
		if (newsListToAdd != null) {
			this.newsList.addAll(newsListToAdd);
			for (int i = 0; i < newsListToAdd.size(); i++) {
				add(newsListToAdd.get(i));
			}
		}
	}

}
