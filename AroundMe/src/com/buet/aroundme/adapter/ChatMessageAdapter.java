package com.buet.aroundme.adapter;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.buet.aroundme.R;
import com.buet.aroundme.model.ChatMessage;
import com.buet.aroundme.util.AppPrefManager;

public class ChatMessageAdapter extends ArrayAdapter<ChatMessage> {

	private final String TAG = this.getClass().getSimpleName();

	private Context thisContext;
	private ArrayList<ChatMessage> chatList;
	private LayoutInflater mInflater;

	public ChatMessageAdapter(Context context, int resource,
			ArrayList<ChatMessage> chatList) {
		super(context, resource, chatList);
		this.chatList = chatList;
		thisContext = context;
		mInflater = (LayoutInflater) thisContext
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		Log.d(TAG, "Constructor done: chatlist.size = " + chatList.size());
	}

	@Override
	public int getCount() {
		return chatList.size();
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		ChatMessage item = (ChatMessage) getItem(position);
		if (convertView == null) {
			if (item.getUid() == AppPrefManager.getUserID(thisContext))
				convertView = mInflater.inflate(R.layout.chat_me_row, null,
						false);
			else
				convertView = mInflater.inflate(R.layout.chat_other_row, null,
						false);
			holder = new ViewHolder();
			holder.tvChatMsg = (TextView) convertView
					.findViewById(R.id.tvChatMsg);
		} else
			holder = (ViewHolder) convertView.getTag();
		try {
			// TODO set
			holder.tvChatMsg.setText(item.getBody());
		} catch (Exception e) {
			Log.e(TAG, "Exception: " + e.toString());
		}
		// TODO set image
		Log.d(TAG, "Chat msg adapter getView() :: News Head=" + item.getBody());

		return convertView;
	}

	private class ViewHolder {
		TextView tvChatMsg;
	}

	public void setData(ArrayList<ChatMessage> newsList) {
		clear();
		if (newsList != null) {
			this.chatList = newsList;
			for (int i = 0; i < newsList.size(); i++) {
				add(newsList.get(i));
			}
		}
	}

	public void addData(ArrayList<ChatMessage> newsListToAdd) {
		if (newsListToAdd != null) {
			this.chatList.addAll(newsListToAdd);
			for (int i = 0; i < newsListToAdd.size(); i++) {
				add(newsListToAdd.get(i));
			}
		}
	}


}
