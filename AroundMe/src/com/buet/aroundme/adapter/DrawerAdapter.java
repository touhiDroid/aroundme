package com.buet.aroundme.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.buet.aroundme.R;
import com.buet.aroundme.model.DrawerItem;

public class DrawerAdapter extends ArrayAdapter<DrawerItem> {

	private final String TAG = this.getClass().getSimpleName();

	private Context context;
	private List<DrawerItem> drawerItemList;
	private int layoutResID;

	public DrawerAdapter(Context context, int layoutResourceID,
			List<DrawerItem> listItems) {
		super(context, layoutResourceID, listItems);
		this.context = context;
		this.drawerItemList = listItems;
		this.layoutResID = layoutResourceID;

	}

	private static class DrawerItemHolder {
		TextView tvItemName;
		ImageView ivIcon;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		DrawerItemHolder drawerHolder;
		View view = convertView;

		if (view == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			drawerHolder = new DrawerItemHolder();

			view = inflater.inflate(layoutResID, parent, false);
			drawerHolder.tvItemName = (TextView) view
					.findViewById(R.id.tvDrawerItemName);
			drawerHolder.ivIcon = (ImageView) view
					.findViewById(R.id.ivDrawerItem);
			// TODO initialize views
			view.setTag(drawerHolder);
		} else {
			drawerHolder = (DrawerItemHolder) view.getTag();
		}

		DrawerItem dItem = (DrawerItem) this.drawerItemList.get(position);
		drawerHolder.ivIcon.setImageDrawable(view.getResources().getDrawable(
				dItem.getImgResId()));
		drawerHolder.tvItemName.setText(dItem.getItemName());
		Log.d(TAG + " :: Getview",
				"Drawer item returned: " + dItem.getItemName());
		return view;
	}
}