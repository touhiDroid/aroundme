/**
 * 
 */
package com.buet.aroundme.model;

/**
 * @author Touhid
 * 
 */
public class DrawerItem {

	private String itemName;
	private int imgResId;

	/**
	 * @param itemName
	 * @param imgResId
	 */
	public DrawerItem(String itemName, int imgResId) {
		super();
		this.itemName = itemName;
		this.imgResId = imgResId;
	}

	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}

	/**
	 * @param itemName
	 *            the itemName to set
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	/**
	 * @return the imgResId
	 */
	public int getImgResId() {
		return imgResId;
	}

	/**
	 * @param imgResId
	 *            the imgResId to set
	 */
	public void setImgResId(int imgResId) {
		this.imgResId = imgResId;
	}

}
