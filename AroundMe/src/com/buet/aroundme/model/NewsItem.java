/**
 * 
 */
package com.buet.aroundme.model;

import java.io.Serializable;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Touhid
 * 
 */
public class NewsItem implements Serializable {
	private static final long serialVersionUID = -1930457341840016914L;
	
	private int id, postedBy, moderatedBy,likeCount, dislikeCount, shareCount, reportCount, commentCount;
	private String updatedAt, createdAt, head, body, imageUrls;
	private double longitude, lattitude;

	/**
	 * @param id
	 * @param postedBy
	 * @param moderatedBy
	 * @param likeCount
	 * @param dislikeCount
	 * @param shareCount
	 * @param reportCount
	 * @param commentCount
	 * @param updatedAt
	 * @param createdAt
	 * @param head
	 * @param body
	 * @param imageUrls
	 * @param longitude
	 * @param lattitude
	 */
	public NewsItem(int id, int postedBy, int moderatedBy, int likeCount,
			int dislikeCount, int shareCount, int reportCount,
			int commentCount, String updatedAt, String createdAt, String head,
			String body, String imageUrls, double longitude, double lattitude) {
		super();
		this.id = id;
		this.postedBy = postedBy;
		this.moderatedBy = moderatedBy;
		this.likeCount = likeCount;
		this.dislikeCount = dislikeCount;
		this.shareCount = shareCount;
		this.reportCount = reportCount;
		this.commentCount = commentCount;
		this.updatedAt = updatedAt;
		this.createdAt = createdAt;
		this.head = head;
		this.body = body;
		this.imageUrls = imageUrls;
		this.longitude = longitude;
		this.lattitude = lattitude;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the postedBy
	 */
	public int getPostedBy() {
		return postedBy;
	}
	/**
	 * @param postedBy the postedBy to set
	 */
	public void setPostedBy(int postedBy) {
		this.postedBy = postedBy;
	}
	/**
	 * @return the moderatedBy
	 */
	public int getModeratedBy() {
		return moderatedBy;
	}
	/**
	 * @param moderatedBy the moderatedBy to set
	 */
	public void setModeratedBy(int moderatedBy) {
		this.moderatedBy = moderatedBy;
	}
	/**
	 * @return the likeCount
	 */
	public int getLikeCount() {
		return likeCount;
	}
	/**
	 * @param likeCount the likeCount to set
	 */
	public void setLikeCount(int likeCount) {
		this.likeCount = likeCount;
	}
	/**
	 * @return the dislikeCount
	 */
	public int getDislikeCount() {
		return dislikeCount;
	}
	/**
	 * @param dislikeCount the dislikeCount to set
	 */
	public void setDislikeCount(int dislikeCount) {
		this.dislikeCount = dislikeCount;
	}
	/**
	 * @return the shareCount
	 */
	public int getShareCount() {
		return shareCount;
	}
	/**
	 * @param shareCount the shareCount to set
	 */
	public void setShareCount(int shareCount) {
		this.shareCount = shareCount;
	}
	/**
	 * @return the reportCount
	 */
	public int getReportCount() {
		return reportCount;
	}
	/**
	 * @param reportCount the reportCount to set
	 */
	public void setReportCount(int reportCount) {
		this.reportCount = reportCount;
	}
	/**
	 * @return the commentCount
	 */
	public int getCommentCount() {
		return commentCount;
	}
	/**
	 * @param commentCount the commentCount to set
	 */
	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}
	/**
	 * @return the updatedAt
	 */
	public String getUpdatedAt() {
		return updatedAt;
	}
	/**
	 * @param updatedAt the updatedAt to set
	 */
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	/**
	 * @return the createdAt
	 */
	public String getCreatedAt() {
		return createdAt;
	}
	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	/**
	 * @return the head
	 */
	public String getHead() {
		return head;
	}
	/**
	 * @param head the head to set
	 */
	public void setHead(String head) {
		this.head = head;
	}
	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}
	/**
	 * @param body the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}
	/**
	 * @return the imageUrls
	 */
	public String getImageUrls() {
		return imageUrls;
	}
	/**
	 * @param imageUrls the imageUrls to set
	 */
	public void setImageUrls(String imageUrls) {
		this.imageUrls = imageUrls;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the lattitude
	 */
	public double getLattitude() {
		return lattitude;
	}

	/**
	 * @param lattitude the lattitude to set
	 */
	public void setLattitude(double lattitude) {
		this.lattitude = lattitude;
	}

	public static ArrayList<NewsItem> parseList(JSONArray jsonArray){
		ArrayList<NewsItem> newsList = new ArrayList<>();

		try {
			for (int i = 0; i < jsonArray.length(); i++) {

				JSONObject j = jsonArray.getJSONObject(i);
				if (j != null) {
					int id = j.getInt("id");
					int pb = j.getInt("posted_by");
					int mb = j.getInt("moderated_by");
					int lc = j.getInt("like_count");
					int dc = j.getInt("dislike_count");
					int sc = j.getInt("share_count");
					int rc = j.getInt("report_count");
					int cc = j.getInt("comment_count");
					String uat = j.getString("updated_at");
					String cat = j.getString("created_at");
					String h = j.getString("head");
					String b = j.getString("body");
					String imgURL = j.getString("image_urls");
					double longi = j.getDouble("longitude");
					double latti = j.getDouble("lattitude");
					NewsItem n = new NewsItem(id, pb, mb, lc, dc, sc, rc, cc, uat, cat, h, b, imgURL, longi, latti);
					newsList.add(n);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		
		return newsList;
	}
	
}
