package com.buet.aroundme.model;

import java.io.Serializable;

import org.json.JSONObject;

public class ServerResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3298748596585139822L;
	JSONObject jObj;
	int status;

	public ServerResponse() {
	}

	public ServerResponse(JSONObject jObj, int status) {
		this.jObj = jObj;
		this.status = status;
	}

	public JSONObject getjObj() {
		return jObj;
	}

	public void setjObj(JSONObject jObj) {
		this.jObj = jObj;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return status + "\n" + jObj.toString();
	}

}
