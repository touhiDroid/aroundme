/**
 * 
 */
package com.buet.aroundme.model;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Touhid
 * 
 */
public class ChatMessage {

	private int id, uid, seen_count;
	private double longi, latti;
	private String updated_at, created_at;
	private String body, img_url;

	/**
	 * @param id
	 * @param uid
	 * @param seen_count
	 * @param longi
	 * @param latti
	 * @param updated_at
	 * @param created_at
	 * @param body
	 * @param img_url
	 */
	public ChatMessage(int id, int uid, int seen_count, double longi,
			double latti, String updated_at, String created_at, String body,
			String img_url) {
		super();
		this.id = id;
		this.uid = uid;
		this.seen_count = seen_count;
		this.setLongitude(longi);
		this.setLattitude(latti);
		this.updated_at = updated_at;
		this.created_at = created_at;
		this.body = body;
		this.img_url = img_url;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the uid
	 */
	public int getUid() {
		return uid;
	}

	/**
	 * @param uid
	 *            the uid to set
	 */
	public void setUid(int uid) {
		this.uid = uid;
	}

	/**
	 * @return the seenCount
	 */
	public int getSeenCount() {
		return seen_count;
	}

	/**
	 * @param seenCount
	 *            the seenCount to set
	 */
	public void setSeenCount(int seenCount) {
		this.seen_count = seenCount;
	}

	/**
	 * @return the updatedAt
	 */
	public String getUpdatedAt() {
		return updated_at;
	}

	/**
	 * @param updatedAt
	 *            the updatedAt to set
	 */
	public void setUpdatedAt(String updatedAt) {
		this.updated_at = updatedAt;
	}

	/**
	 * @return the createdAt
	 */
	public String getCreatedAt() {
		return created_at;
	}

	/**
	 * @param createdAt
	 *            the createdAt to set
	 */
	public void setCreatedAt(String createdAt) {
		this.created_at = createdAt;
	}

	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @param body
	 *            the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}

	/**
	 * @return the imgUrl
	 */
	public String getImgUrl() {
		return img_url;
	}

	/**
	 * @param imgUrl
	 *            the imgUrl to set
	 */
	public void setImgUrl(String imgUrl) {
		this.img_url = imgUrl;
	}

	/**
	 * @return the longi
	 */
	public double getLongitude() {
		return longi;
	}

	/**
	 * @param longi the longi to set
	 */
	public void setLongitude(double longi) {
		this.longi = longi;
	}

	public static ArrayList<ChatMessage> parseList(JSONArray jsonArray) {
		ArrayList<ChatMessage> chatList = new ArrayList<>();

		try {
			for (int i = 0; i < jsonArray.length(); i++) {

				JSONObject j = jsonArray.getJSONObject(i);
				if (j != null) {
					int id = 0, uid = 0, seen_count = 0;
					double longi = 0, latti = 0;
					String updated_at = "", created_at = "", body = "", img_url = "";
					ChatMessage cm = new ChatMessage(id, uid, seen_count,
							longi, latti, updated_at, created_at, body, img_url);
					chatList.add(cm);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return chatList;
	}

	/**
	 * @return the latti
	 */
	public double getLattitude() {
		return latti;
	}

	/**
	 * @param latti the latti to set
	 */
	public void setLattitude(double latti) {
		this.latti = latti;
	}
}
