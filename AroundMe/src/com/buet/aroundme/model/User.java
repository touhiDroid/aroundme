package com.buet.aroundme.model;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author Touhid
 * 
 */
public class User {

	private int id, gender, points, level;

	private double last_longi, last_latti;
	private String updated_at, created_at, name, password, email, dob,
			contact_no, address, pic_url, gcm_id, xtoken, last_imei, last_ip,
			last_mac, last_login;

	/**
	 * @param id
	 * @param gender
	 * @param points
	 * @param level
	 * @param last_longi
	 * @param last_latti
	 * @param updated_at
	 * @param created_at
	 * @param name
	 * @param password
	 * @param email
	 * @param dob
	 * @param contact_no
	 * @param address
	 * @param pic_url
	 * @param gcm_id
	 * @param xtoken
	 * @param last_imei
	 * @param last_ip
	 * @param last_mac
	 * @param last_login
	 */
	public User(int id, int gender, int points, int level, double last_longi,
			double last_latti, String updated_at, String created_at,
			String name, String password, String email, String dob,
			String contact_no, String address, String pic_url, String gcm_id,
			String xtoken, String last_imei, String last_ip, String last_mac,
			String last_login) {
		super();
		this.id = id;
		this.gender = gender;
		this.points = points;
		this.level = level;
		this.last_longi = last_longi;
		this.last_latti = last_latti;
		this.updated_at = updated_at;
		this.created_at = created_at;
		this.name = name;
		this.password = password;
		this.email = email;
		this.dob = dob;
		this.contact_no = contact_no;
		this.address = address;
		this.pic_url = pic_url;
		this.gcm_id = gcm_id;
		this.xtoken = xtoken;
		this.last_imei = last_imei;
		this.last_ip = last_ip;
		this.last_mac = last_mac;
		this.last_login = last_login;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the gender
	 */
	public int getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(int gender) {
		this.gender = gender;
	}

	/**
	 * @return the points
	 */
	public int getPoints() {
		return points;
	}

	/**
	 * @param points
	 *            the points to set
	 */
	public void setPoints(int points) {
		this.points = points;
	}

	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * @param level
	 *            the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * @return the last_longi
	 */
	public double getLast_longi() {
		return last_longi;
	}

	/**
	 * @param last_longi
	 *            the last_longi to set
	 */
	public void setLast_longi(double last_longi) {
		this.last_longi = last_longi;
	}

	/**
	 * @return the last_latti
	 */
	public double getLast_latti() {
		return last_latti;
	}

	/**
	 * @param last_latti
	 *            the last_latti to set
	 */
	public void setLast_latti(double last_latti) {
		this.last_latti = last_latti;
	}

	/**
	 * @return the updated_at
	 */
	public String getUpdated_at() {
		return updated_at;
	}

	/**
	 * @param updated_at
	 *            the updated_at to set
	 */
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	/**
	 * @return the created_at
	 */
	public String getCreated_at() {
		return created_at;
	}

	/**
	 * @param created_at
	 *            the created_at to set
	 */
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the dob
	 */
	public String getDob() {
		return dob;
	}

	/**
	 * @param dob
	 *            the dob to set
	 */
	public void setDob(String dob) {
		this.dob = dob;
	}

	/**
	 * @return the contact_no
	 */
	public String getContact_no() {
		return contact_no;
	}

	/**
	 * @param contact_no
	 *            the contact_no to set
	 */
	public void setContact_no(String contact_no) {
		this.contact_no = contact_no;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the pic_url
	 */
	public String getPic_url() {
		return pic_url;
	}

	/**
	 * @param pic_url
	 *            the pic_url to set
	 */
	public void setPic_url(String pic_url) {
		this.pic_url = pic_url;
	}

	/**
	 * @return the gcm_id
	 */
	public String getGcm_id() {
		return gcm_id;
	}

	/**
	 * @param gcm_id
	 *            the gcm_id to set
	 */
	public void setGcm_id(String gcm_id) {
		this.gcm_id = gcm_id;
	}

	/**
	 * @return the xtoken
	 */
	public String getXtoken() {
		return xtoken;
	}

	/**
	 * @param xtoken
	 *            the xtoken to set
	 */
	public void setXtoken(String xtoken) {
		this.xtoken = xtoken;
	}

	/**
	 * @return the last_imei
	 */
	public String getLast_imei() {
		return last_imei;
	}

	/**
	 * @param last_imei
	 *            the last_imei to set
	 */
	public void setLast_imei(String last_imei) {
		this.last_imei = last_imei;
	}

	/**
	 * @return the last_ip
	 */
	public String getLast_ip() {
		return last_ip;
	}

	/**
	 * @param last_ip
	 *            the last_ip to set
	 */
	public void setLast_ip(String last_ip) {
		this.last_ip = last_ip;
	}

	/**
	 * @return the last_mac
	 */
	public String getLast_mac() {
		return last_mac;
	}

	/**
	 * @param last_mac
	 *            the last_mac to set
	 */
	public void setLast_mac(String last_mac) {
		this.last_mac = last_mac;
	}

	/**
	 * @return the last_login
	 */
	public String getLast_login() {
		return last_login;
	}

	/**
	 * @param last_login
	 *            the last_login to set
	 */
	public void setLast_login(String last_login) {
		this.last_login = last_login;
	}

	public static User parseUser(String data) {
		GsonBuilder gsonb = new GsonBuilder();
		Gson gson = gsonb.create();
		JSONObject jo;
		try {
			jo = new JSONObject(data);
			if (jo != null) {
				String jsonString = jo.toString();
				User us = gson.fromJson(jsonString, User.class);
				Log.d("User", us.toString());
				return us;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [id=" + id + ", gender=" + gender + ", points=" + points
				+ ", level=" + level + ", last_longi=" + last_longi
				+ ", last_latti=" + last_latti + ", updated_at=" + updated_at
				+ ", created_at=" + created_at + ", name=" + name
				+ ", password=" + password + ", email=" + email + ", dob="
				+ dob + ", contact_no=" + contact_no + ", address=" + address
				+ ", pic_url=" + pic_url + ", gcm_id=" + gcm_id + ", xtoken="
				+ xtoken + ", last_imei=" + last_imei + ", last_ip=" + last_ip
				+ ", last_mac=" + last_mac + ", last_login=" + last_login + "]";
	}

}
