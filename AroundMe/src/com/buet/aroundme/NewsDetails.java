/**
 * 
 */
package com.buet.aroundme;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.androidquery.AQuery;
import com.buet.aroundme.model.NewsItem;
import com.buet.aroundme.util.AppConstants;

/**
 * @author Touhid
 * 
 */
public class NewsDetails extends SherlockActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.news_details);

		NewsItem ni = (NewsItem) getIntent().getSerializableExtra("news");

		TextView tvHead = (TextView) findViewById(R.id.tvHeadNewsDetails);
		TextView tvBody = (TextView) findViewById(R.id.tvBodyNewsDetails);
		TextView tvTime = (TextView) findViewById(R.id.tvTimeNewsDetails);

		tvHead.setText(ni.getHead());
		tvBody.setText(ni.getBody());
		tvTime.setText(ni.getUpdatedAt());

		ImageView ivNews = (ImageView) findViewById(R.id.ivNewsDetails);
		String imgURL = AppConstants.API_URL + ni.getImageUrls();
		Bitmap preset = BitmapFactory.decodeResource(getResources(),
				R.drawable.banner_placeholder);
		AQuery aq = new AQuery(this);
		aq.id(ivNews).image(imgURL, true, true, 0,
				R.drawable.banner_placeholder, preset, AQuery.FADE_IN,
				AQuery.RATIO_PRESERVE);

	}
}
