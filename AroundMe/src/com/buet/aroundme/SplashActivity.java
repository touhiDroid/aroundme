/**
 * 
 */
package com.buet.aroundme;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import com.actionbarsherlock.app.SherlockActivity;
import com.buet.aroundme.model.ServerResponse;
import com.buet.aroundme.model.User;
import com.buet.aroundme.parser.JsonParser;
import com.buet.aroundme.util.AppConstants;
import com.buet.aroundme.util.AppPrefManager;
import com.buet.aroundme.util.AppUtils;
import com.splunk.mint.Mint;

/**
 * @author Touhid
 * 
 */
public class SplashActivity extends SherlockActivity {

	// private final int SPLASH_TIME_OUT = 2500;
	private final String TAG = "SplashActivity";

	private Location location;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Mint.initAndStartSession(SplashActivity.this, AppConstants.MINT_API_KEY);
		setContentView(R.layout.splash);
		// final Intent intent = new Intent(SplashActivity.this,
		// MainActivity.class);
		location = AppUtils.getLocation(this);
		// new Handler().postDelayed(new Runnable() {
		// /**
		// * Showing splash screen with a timer. This will be useful when you
		// * want to show case your app logo / company
		// */
		// @Override
		// public void run() {
		// Intent intent = new Intent(SplashScreen.this,
		// UserCheck.class);
		// intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		// startActivity(intent);
		// finish();
		// }
		// }, SPLASH_TIME_OUT);

		// startActivity(intent);
		// finish();

		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		if (AppUtils.isInternetAvailable(this))
			new AccessApp().execute();
		else
			AppUtils.showNoInternetAlert(this);
	}

	private class AccessApp extends AsyncTask<Integer, Void, JSONObject> {

		private JsonParser jsonParser;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (jsonParser == null)
				jsonParser = new JsonParser();
		}

		@Override
		protected JSONObject doInBackground(Integer... params) {

			JSONObject jObj = new JSONObject();
			try {
				jObj.put("request", AppConstants.REQ_USER_ACCESS);
				jObj.put("longi", location.getLongitude());
				jObj.put("latti", location.getLatitude());
				jObj.put("imei", AppUtils.getDeviceIMEI(SplashActivity.this));
			} catch (JSONException e) {
				e.printStackTrace();
			}

			ServerResponse response;
			try {
				response = jsonParser.retrieveServerData(jObj.toString());
			} catch (ConnectTimeoutException e) {
				e.printStackTrace();
				AppUtils.toast(SplashActivity.this, "Connection is timed out!");
				return null;
			}
			if (response != null && response.getStatus() == 200) {
				Log.d(">>>><<<<", "success in retrieving user.");
				return response.getjObj();
			} else
				return null;
		}

		@Override
		protected void onPostExecute(JSONObject responseObj) {
			super.onPostExecute(responseObj);
			if (responseObj != null) {
				try {
					String status = responseObj.getString(
							AppConstants.REQ_STATUS).trim();
					Log.d(TAG, "Status = " + status);
					if (status.equals(AppConstants.REQ_SUCCESS)) {
						String data = responseObj.getString("data");
						if (data.length() <= 8) {
							Intent intent = new Intent(SplashActivity.this,
									LogIn.class);
							intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(intent);
							finish();
						} else {
							autoLogInUser(data);
						}
					} else {
					}
				} catch (JSONException e) {
					Log.w(TAG, "Malformed data received!");
					AppUtils.toast(SplashActivity.this,
							"Malformed data received!");
					e.printStackTrace();
				}
			}
		}
	}

	public void autoLogInUser(String data) {
		AppPrefManager.setUserData(this, User.parseUser(data));
		Intent intent = new Intent(this, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		finish();
	}
}
