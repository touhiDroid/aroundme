package com.buet.aroundme;

import java.util.ArrayList;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.buet.aroundme.adapter.DrawerAdapter;
import com.buet.aroundme.fragment.About;
import com.buet.aroundme.fragment.ChatCurRoom;
import com.buet.aroundme.fragment.FAQ;
import com.buet.aroundme.fragment.NewsCurLocation;
import com.buet.aroundme.fragment.Settings;
import com.buet.aroundme.model.DrawerItem;
import com.buet.aroundme.services.GPSSenderService;
import com.buet.aroundme.util.AppConstants;
import com.buet.aroundme.util.AppUtils;
import com.buet.aroundme.util.SherlockActionBarDrawerToggle;
import com.google.android.gcm.GCMRegistrar;
import com.splunk.mint.Mint;

public class MainActivity extends SherlockFragmentActivity {
	private static final String TAG = MainActivity.class.getSimpleName();

	// GCM Registration components
	private GCMController gcmController;
	private AsyncTask<Void, Void, Void> mRegisterTask;

	private static DrawerLayout mDrawerLayout;
	private static ListView mDrawerList;
	private SherlockActionBarDrawerToggle mDrawerToggler;

	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	private DrawerAdapter adapter;

	private static List<DrawerItem> dataList;
	private static ArrayList<SherlockFragment> fragList;
	private static FragmentManager fragManager;

	private static Context tContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Mint.initAndStartSession(MainActivity.this, AppConstants.MINT_API_KEY);
		setContentView(R.layout.activity_main);
		configDrawer();
		initFragments();
		tContext = MainActivity.this;

		// TODO Check calls from notification click - news, chat or ad fragment
		// inside intent-extra
		fragManager = getSupportFragmentManager();
		selectMenu(0);
		this.startService(new Intent(this, GPSSenderService.class));
		initiateGCMRegistrationProcess();
	}

	private void initiateGCMRegistrationProcess() {
		// Get Global Controller Class object (see application tag in
		// AndroidManifest.xml)
		gcmController = new GCMController(this);

		// Check if GCM configuration is set
		if (AppConstants.GOOGLE_PROJECT_ID.equals(null)
				|| AppConstants.GOOGLE_PROJECT_ID.length() == 0) {
			// GCM sernder id is missing
			Log.e(TAG, "Configuration Error!Please set your GCM Sender ID");
			return;
		}
		// Check if Internet Connection present
		if (!AppUtils.isInternetAvailable(this)) {
			Log.e(TAG, "ABORTING GCM registration for no int.-connection");
			return;
		}
		// Check if user filled the form
		registerUser();
	}

	protected void registerUser() {
		try {
			// Make sure the device has the proper dependencies.
			GCMRegistrar.checkDevice(this);
			// Make sure the manifest permissions was properly set
			GCMRegistrar.checkManifest(this);
		} catch (Exception e) {
			AppUtils.showSimpleAlert(
					MainActivity.this,
					"Your device doesn't support instant notification!\nYou'll MISS a LOT from this app :(");
		}
		// Register custom Broadcast receiver to show messages on activity
		registerReceiver(mHandleMessageReceiver, new IntentFilter(
				AppConstants.GCM_DISPLAY_MSG_ACTION));

		// Get GCM registration id
		Log.d(TAG, "Getting the registration id ...");
		final String regId = GCMRegistrar.getRegistrationId(this);
		Log.i(TAG, "Got the registration id: " + regId);

		// Check if regid already presents
		if (regId.equals(null) || regId.equals("") || regId.length() == 0) {
			// Register with GCM
			Log.e(TAG, "Doing a new registration for empty regId: " + regId);
			GCMRegistrar.register(this, AppConstants.GOOGLE_PROJECT_ID);
		} else {
			// Device is already registered on GCM-Server
			Log.e(TAG, "Device already registered with regId: " + regId);
			if (GCMRegistrar.isRegisteredOnServer(this)) {
				// Skips registration.
				Log.i(TAG,
						"Device already registered also in the server with regId: "
								+ regId);
			} else {
				// Try to register again, but not in the UI thread.
				// It's also necessary to cancel the thread onDestroy(),
				// hence the use of AsyncTask instead of a raw thread.
				Log.e(TAG, "GCM-registered, but not in the server with regId: "
						+ regId);
				mRegisterTask = new AsyncTask<Void, Void, Void>() {
					@Override
					protected Void doInBackground(Void... params) {
						// Register (save the regId as gcm_id) on our server
						Log.i("mRegisterTask : doInBackground",
								"registering: regId: " + regId);
						gcmController.register(MainActivity.this, regId);
						return null;
					}

					@Override
					protected void onPostExecute(Void result) {
						if (mRegisterTask != null)
							mRegisterTask.cancel(true);
						mRegisterTask = null;
					}
				};
				// execute AsyncTask
				mRegisterTask.execute();
			}
		}
	}

	// Create a broadcast receiver to get message and show on screen
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d(TAG, "mHandleMessageReceiver : msg received.");

			String newMessage = intent.getExtras().getString("message");
			int targetId = intent.getExtras().getInt("target_id");
			int type = intent.getExtras().getInt("type");
			Log.d(TAG, "mHandleMessageReceiver : msg:" + newMessage);

			// Waking up mobile if it is sleeping
			gcmController.acquireWakeLock(getApplicationContext());
			Toast.makeText(context, "Got Message: " + newMessage,
					Toast.LENGTH_LONG).show();
			AppUtils.generateNotification(context, newMessage, type, targetId);
			// Releasing wake lock
			gcmController.releaseWakeLock();
		}
	};

	protected void onDestroy() {
		// Cancel AsyncTask
		if (mRegisterTask != null) {
			mRegisterTask.cancel(true);
			mRegisterTask = null;
		}
		try {
			// Unregister Broadcast Receiver
			unregisterReceiver(mHandleMessageReceiver);
			// Clear internal resources.
			GCMRegistrar.onDestroy(this);
		} catch (Exception e) {
			Log.e("UnRegister Receiver Error", "> " + e.getMessage());
		}
		super.onDestroy();
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.news_post, menu);
		return true;
	}

	private void configDrawer() {// Initializing
		final ActionBar actionBar = getSupportActionBar();

		dataList = new ArrayList<DrawerItem>();
		mTitle = mDrawerTitle = getTitle();
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);

		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);

		// Add Drawer Item to dataList
		dataList.add(new DrawerItem("News", R.drawable.ic_launcher));// 0
		dataList.add(new DrawerItem("Chat Room", R.drawable.ic_chat_icon));// 1
		dataList.add(new DrawerItem("My Porfile", R.drawable.ic_user_b));// 2
		dataList.add(new DrawerItem("Settings", R.drawable.ic_settings));// 3
		dataList.add(new DrawerItem("FAQ", R.drawable.ic_help));// 4
		dataList.add(new DrawerItem("About", R.drawable.action_about));// 5
		dataList.add(new DrawerItem("Log Out", R.drawable.ic_launcher));// 6

		adapter = new DrawerAdapter(this, R.layout.drawer_row, dataList);
		mDrawerList.setAdapter(adapter);

		mDrawerList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Log.d(TAG, "selectMenu : " + position);
				if (position < 2)// fragList.size()
					selectMenu(position);
				else if (position == 2) {
					mDrawerList.setItemChecked(position, true);
					mDrawerLayout.closeDrawer(mDrawerList);
					gotoMyProfile();
				} else if (position < 5) {
					selectMenu(position - 1);
				}
			}
		});

		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);

		mDrawerToggler = new SherlockActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close) {
			public void onDrawerClosed(View view) {
				actionBar.setTitle(mTitle);
				supportInvalidateOptionsMenu(); // creates call to
				// onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				actionBar.setTitle(mDrawerTitle);
				supportInvalidateOptionsMenu(); // creates call to
				// onPrepareOptionsMenu()
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggler);
	}

	private void initFragments() {

		fragList = new ArrayList<>();
		SherlockFragment fragment = null;
		Bundle args = new Bundle();

		// 0
		fragment = NewsCurLocation.newInstance();// News.newInstance();
		args.putString(AppConstants.DRAWER_ITEM_NAME, dataList.get(0)
				.getItemName());
		args.putInt(AppConstants.IMAGE_RESOURCE_ID, dataList.get(0)
				.getImgResId());
		fragment.setArguments(args);
		fragList.add(fragment);

		// 1
		fragment = ChatCurRoom.newInstance();
		args.putString(AppConstants.DRAWER_ITEM_NAME, dataList.get(1)
				.getItemName());
		args.putInt(AppConstants.IMAGE_RESOURCE_ID, dataList.get(1)
				.getImgResId());
		fragment.setArguments(args);
		fragList.add(fragment);

		// 2
		// fragment = MyProfile.newInstance();
		// args.putString(AppConstants.DRAWER_ITEM_NAME, dataList.get(2)
		// .getItemName());
		// args.putInt(AppConstants.IMAGE_RESOURCE_ID, dataList.get(2)
		// .getImgResId());
		// fragment.setArguments(args);
		// fragList.add(fragment);

		// 2
		fragment = Settings.newInstance();
		args.putString(AppConstants.DRAWER_ITEM_NAME, dataList.get(3)
				.getItemName());
		args.putInt(AppConstants.IMAGE_RESOURCE_ID, dataList.get(3)
				.getImgResId());
		fragment.setArguments(args);
		fragList.add(fragment);

		// 3
		fragment = FAQ.newInstance();
		args.putString(AppConstants.DRAWER_ITEM_NAME, dataList.get(4)
				.getItemName());
		args.putInt(AppConstants.IMAGE_RESOURCE_ID, dataList.get(4)
				.getImgResId());
		fragment.setArguments(args);
		fragList.add(fragment);

		// 4
		fragment = About.newInstance();
		args.putString(AppConstants.DRAWER_ITEM_NAME, dataList.get(5)
				.getItemName());
		args.putInt(AppConstants.IMAGE_RESOURCE_ID, dataList.get(5)
				.getImgResId());
		fragment.setArguments(args);
		fragList.add(fragment);

		Log.d(TAG, "Frag-list is done");
	}

	private void selectMenu(int position) {
		Log.d(TAG, "selectMenu : " + position);
		SherlockFragment fragment = fragList.get(position);
		fragManager.beginTransaction().replace(R.id.content_frame, fragment)
				.commit();

		mDrawerList.setItemChecked(position, true);
		setTitle(dataList.get(position).getItemName());
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	public static void selectMenuStatically(int position) {
		Log.d(TAG, "selectMenu : " + position);
		SherlockFragment fragment = fragList.get(position);
		fragManager.beginTransaction().replace(R.id.content_frame, fragment)
				.commit();

		mDrawerList.setItemChecked(position, true);
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getSupportActionBar().setTitle(mTitle);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggler.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggles
		mDrawerToggler.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(
			com.actionbarsherlock.view.MenuItem item) {
		// The action bar home/up action should open or close the drawer.
		// ActionBarDrawerToggle will take care of this.
		switch (item.getItemId()) {
		case android.R.id.home:
			return mDrawerToggler.onOptionsItemSelected(item);
		case R.id.action_post:
			Intent intent = new Intent(this, Post.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			return true;
		default:
			return false;
		}
	}

	public static void gotoMyProfile() {
		mDrawerList.setItemChecked(2, true);
		mDrawerLayout.closeDrawer(mDrawerList);
		if (tContext != null) {
			Intent im = new Intent(tContext, MyProfile.class);
			tContext.startActivity(im);
		} else {
			Log.e(TAG, "Can't go to MyProfile coz tContext is null");
		}
	}

	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		super.onActivityResult(arg0, arg1, arg2);
	}
}
