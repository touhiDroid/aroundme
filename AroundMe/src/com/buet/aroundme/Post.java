/**
 * 
 */
package com.buet.aroundme;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.buet.aroundme.model.ServerResponse;
import com.buet.aroundme.parser.JsonParser;
import com.buet.aroundme.util.AppConstants;
import com.buet.aroundme.util.AppPrefManager;
import com.buet.aroundme.util.AppUtils;
import com.buet.aroundme.util.Base64;

import eu.janmuller.android.simplecropimage.CropImage;

/**
 * @author Touhid
 * 
 */
public class Post extends SherlockActivity implements OnClickListener {
	private static final String TAG = "Post";

	private static final int CAMERA_REQ_CODE = 901;
	private static final int CROP_REQ_CODE = 1001;
	private static final int GALLERY_REQ_CODE = 902;

	private File picFile;
	private Uri mImageCaptureUri;
	private Bitmap scaledBmp;
	private String imgB64Str;
	private ProgressDialog pDialog;

	private Button btnPic, btnPost;// btnLoc,
	private ImageView ivNews;
	private EditText etHead, etBody;

	private String head, body;
	private Location location;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.news_post);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		etHead = (EditText) findViewById(R.id.etHeadNewsPost);
		etBody = (EditText) findViewById(R.id.etNewPost);

		btnPic = (Button) findViewById(R.id.btnAttachPic);
		// btnLoc = (Button) findViewById(R.id.btnDiffLocation);
		btnPost = (Button) findViewById(R.id.btnPost);
		ivNews = (ImageView) findViewById(R.id.ivNewsImage);

		btnPic.setOnClickListener(this);
		// btnLoc.setOnClickListener(this);
		btnPost.setOnClickListener(this);
		ivNews.setOnClickListener(this);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void showPicTakerDialog() {
		final Dialog dialog = new Dialog(Post.this,
				android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
		dialog.setContentView(R.layout.pic_taker_dialog);
		dialog.findViewById(R.id.btn_camera_pic_taker_dlg).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.cancel();
						onClickCameraCapture();
					}
				});
		dialog.findViewById(R.id.btn_browse_pic_taker_dlg).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.cancel();
						setFileName();
						Intent intent = new Intent(Intent.ACTION_PICK);
						intent.setType("image/*");
						startActivityForResult(intent, GALLERY_REQ_CODE);
					}
				});

		// Center-focus the dialog
		Window window = dialog.getWindow();
		window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);

		// The below code is EXTRA - to dim the parent view by 70% :D
		WindowManager.LayoutParams lp = window.getAttributes();
		lp.dimAmount = 0.7f;
		lp.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		dialog.getWindow().setAttributes(lp);
		dialog.show();
	}

	private void setFileName() {
		// Set the file name
		String appDir = AppConstants.APP_DIRECTORY.toString();
		String photoFileName = "ame/news_" + System.currentTimeMillis()
				+ ".jpg";
		Log.d(TAG, photoFileName);
		picFile = new File(appDir, photoFileName);
		if (!picFile.exists()) {
			try {
				if (!AppConstants.APP_DIRECTORY.exists())
					AppConstants.APP_DIRECTORY.mkdirs();
				if (picFile.mkdirs() && picFile.delete()
						&& picFile.createNewFile())
					Log.d(TAG, "PicFile newly created");
			} catch (IOException e) {
				Log.d(TAG, "Exception for new creation of picFile");
				e.printStackTrace();
			}
		} else {
			Log.d(TAG, "picFile exists. Replacing it...");
			if (!picFile.delete())
				Log.d(TAG, "picFile exists. Deletion failed!");
			else
				setFileName();
		}
	}

	private void onClickCameraCapture() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		Log.d(TAG, "cam clicked");
		try {
			setFileName();
			// set uri from the file
			// mImageCaptureUri = Uri.fromFile(picFile);
			if (AppUtils.isSDCardMounted()) {
				mImageCaptureUri = Uri.fromFile(picFile);
			} else {
				Toast.makeText(Post.this, "Media Not Mounted!",
						Toast.LENGTH_LONG).show();
				return;
			}
			intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
			intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT,
					mImageCaptureUri);
			intent.putExtra("return-data", true);
			Log.d(TAG, "cam intent starting");
			startActivityForResult(intent, CAMERA_REQ_CODE);
		} catch (ActivityNotFoundException e) {
			Log.d("Error", "Activity Not Found: " + e.toString());
		}
	}

	private void startCropImage() {
		Intent intent = new Intent(this, CropImage.class);
		intent.putExtra(CropImage.IMAGE_PATH, picFile.getPath());
		intent.putExtra(CropImage.SCALE, true);

		intent.putExtra(CropImage.ASPECT_X, 5);
		intent.putExtra(CropImage.ASPECT_Y, 4);

		Log.d(TAG, "Starting crop from startCropImage()");
		startActivityForResult(intent, CROP_REQ_CODE);
	}

	private void loadPicasaImageFromGallery(final Uri uri) {
		String[] projection = { MediaColumns.DATA, MediaColumns.DISPLAY_NAME };
		Cursor cursor = getContentResolver().query(uri, projection, null, null,
				null);
		if (cursor != null) {
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(MediaColumns.DISPLAY_NAME);
			if (columnIndex != -1) {
				new Thread(new Runnable() {
					public void run() {
						try {
							Bitmap bitmap = android.provider.MediaStore.Images.Media
									.getBitmap(getContentResolver(), uri);
							int h = 200 * (bitmap.getHeight() / bitmap
									.getWidth());
							if (h <= 10)
								h = 100;
							scaledBmp = Bitmap.createScaledBitmap(bitmap, 200,
									h, true);

							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									try {
										FileOutputStream out = new FileOutputStream(
												picFile);
										scaledBmp.compress(
												Bitmap.CompressFormat.PNG, 90,
												out);
										mImageCaptureUri = Uri
												.fromFile(picFile);
										out.close();
										// ivProfilePic.setImageBitmap(scaledBmp);
										// btnUpdate.setVisibility(View.VISIBLE);
									} catch (FileNotFoundException e) {
										e.printStackTrace();
									} catch (IOException e) {
										e.printStackTrace();
									}
								}
							});

						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
				}).start();
			}
		}
		cursor.close();

	}

	private void showPicDeleteDialog() {
		Builder deleteDialog = new Builder(this);
		deleteDialog.setTitle("Delete this picture?");
		deleteDialog.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						if (picFile.delete()) {
							Toast.makeText(Post.this, "Picture deleted!",
									Toast.LENGTH_SHORT).show();
							imgB64Str = "";
							ivNews.setVisibility(View.GONE);
						}
					}
				});
		deleteDialog.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		deleteDialog.show();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnAttachPic:
			showPicTakerDialog();
			break;
		// case R.id.btnDiffLocation:
		// showPicTakerDialog();
		// break;
		case R.id.btnPost:
			// Toast.makeText(this, "The post will be verified & posted",
			// Toast.LENGTH_LONG).show();
			if(!AppPrefManager.isUSerProfileComplete(Post.this)) {
				AppUtils.showProfileCompletionPrompt(Post.this);
				return;
			}
			head = etHead.getText().toString();
			body = etBody.getText().toString();
			if (isInputValid()) {
				showProgressDialog();
				Log.i(TAG, "Post values are ok ... now posting");
				// location = AppUtils.getLocation(Post.this);
				if (location == null)
					location = new Location(LocationManager.NETWORK_PROVIDER);
				Log.i(TAG, "Loc. : " + location.getLongitude() + ", "
						+ location.getLatitude());
				new NewsPostTask().execute();
			}
			break;
		case R.id.ivNewsImage:
			showPicDeleteDialog();
			break;

		default:
			break;
		}
	}

	private void showProgressDialog() {
		if (pDialog == null)
			pDialog = new ProgressDialog(Post.this);
		if (!pDialog.isShowing()) {
			pDialog.setMessage("Posting your news ...");
			pDialog.setCancelable(false);
			pDialog.setIndeterminate(true);
			pDialog.show();
		}
	}

	private boolean isInputValid() {
		if (head.equals(null) || head.equals("") || head.length() <= 0)
			toast("Headline can't be left blank!");
		else if (body.equals(null) || body.equals("") || body.length() <= 0)
			toast("News body can't be left blank!");
		else
			return true;
		return false;
	}

	private void toast(final String msg) {
		if (Looper.getMainLooper() == Looper.myLooper())
			Toast.makeText(Post.this, msg, Toast.LENGTH_LONG).show();
		else
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(Post.this, msg, Toast.LENGTH_LONG).show();
				}
			});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case CAMERA_REQ_CODE:
				Log.d(TAG, "cam onActivityResult");
				try {
					Bitmap bmp = AppUtils.decodeFileToBitmap(picFile, 500);
					int h = 200 * (bmp.getHeight() / bmp.getWidth());
					if (h <= 10)
						h = 100;
					int angle = AppUtils.getCorrectionAngleForCam(picFile);
					if (angle == 0) {
						scaledBmp = Bitmap
								.createScaledBitmap(bmp, 200, h, true);
					} else {
						Matrix mat = new Matrix();
						mat.postRotate(angle);
						Bitmap correctBmp = Bitmap.createBitmap(bmp, 0, 0,
								bmp.getWidth(), bmp.getHeight(), mat, true);
						scaledBmp = Bitmap.createScaledBitmap(correctBmp, 200,
								h, true);
						Log.d("", "scaled");
					}

					try {
						FileOutputStream out = new FileOutputStream(picFile);
						scaledBmp.compress(Bitmap.CompressFormat.PNG, 90, out);
						mImageCaptureUri = Uri.fromFile(picFile);
						out.close();
					} catch (Exception e) {
						Log.e("Error_Touhid", e.toString());
					}
					// code b4 crop: ivProfilePic.setImageBitmap(scaledBmp);
					// btnUpdate.setVisibility(View.VISIBLE);
				} catch (IOException e) {
					Toast.makeText(Post.this, "IOException - Failed to load",
							Toast.LENGTH_SHORT).show();
					Log.e("Camera", e.toString());
				} catch (OutOfMemoryError oom) {
					Toast.makeText(Post.this, "OOM error - Failed to load",
							Toast.LENGTH_SHORT).show();
					Log.e("Camera", oom.toString());
				}
				Log.d(TAG, "starting crop");
				startCropImage();
				//
				// doCrop();
				// imageUriToString = mImageCaptureUri.toString();
				break;

			case GALLERY_REQ_CODE:
				try {
					mImageCaptureUri = data.getData();

					if (AppUtils.getPathFromURI(Post.this, mImageCaptureUri) != null) {
						try {
							InputStream inputStream = getContentResolver()
									.openInputStream(mImageCaptureUri);
							FileOutputStream fileOutputStream = new FileOutputStream(
									picFile);
							AppUtils.copyI2OStream(inputStream,
									fileOutputStream);
							fileOutputStream.close();
							inputStream.close();
						} catch (FileNotFoundException e) {
							System.out.println("Picasa Image,"
									+ "coz FileNotFoundException!");
							loadPicasaImageFromGallery(mImageCaptureUri);
						}
					} else {
						System.out.println("Picasa Image!");
						loadPicasaImageFromGallery(mImageCaptureUri);
					}
					Log.d(TAG, "Gal code: starting crop");
					startCropImage();

				} catch (Exception e) {
					Log.e(TAG, "Error while creating the picture file", e);
				}
				break;

			case CROP_REQ_CODE:
				String path = data.getStringExtra(CropImage.IMAGE_PATH);
				Log.d(TAG, "path = " + path);
				if (path == null) {
					return;
				}
				scaledBmp = BitmapFactory.decodeFile(picFile.getPath());
				int h = 200 * (scaledBmp.getHeight() / scaledBmp.getWidth());
				if (h <= 10)
					h = 100;
				scaledBmp = Bitmap.createScaledBitmap(scaledBmp, 200, h, true);
				ivNews.setVisibility(View.VISIBLE);
				ivNews.setImageBitmap(scaledBmp);
				ByteArrayOutputStream bao = new ByteArrayOutputStream();
				scaledBmp.compress(Bitmap.CompressFormat.JPEG, 90, bao);
				byte[] ba = bao.toByteArray();
				imgB64Str = Base64.encodeToString(ba, Base64.DEFAULT);
				break;
			}
		}
	}

	private class NewsPostTask extends AsyncTask<Void, Void, JSONObject> {

		private JsonParser jsonParser;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (jsonParser == null)
				jsonParser = new JsonParser();
		}

		@Override
		protected JSONObject doInBackground(Void... params) {

			JSONObject jObj = new JSONObject();
			try {
				jObj.put("request", AppConstants.REQ_NEWS_POST);
				jObj.put("head", head);
				jObj.put("body", body);
				jObj.put("img", imgB64Str);
				jObj.put("longi", location.getLongitude());
				jObj.put("latti", location.getLatitude());
				// jObj.put("lat", );
			} catch (JSONException e) {
				e.printStackTrace();
			}

			ServerResponse response;
			try {
				response = jsonParser.retrieveServerData(jObj.toString());
			} catch (ConnectTimeoutException e) {
				e.printStackTrace();
				toast("Connection is timed out!");
				return null;
			}
			if (response.getStatus() == 200) {
				Log.d(">>>><<<<", "success in retrieving notifications.");
				JSONObject responseObj = response.getjObj();
				return responseObj;
			} else
				return null;
		}

		@Override
		protected void onPostExecute(JSONObject responseObj) {
			super.onPostExecute(responseObj);
			if (responseObj != null) {
				try {
					String status = responseObj
							.getString(AppConstants.REQ_STATUS);
					if (status.equals(AppConstants.REQ_SUCCESS)) {
						Log.d(TAG, "Status is ok");
						AppUtils.toast(Post.this, "Your news is posted :)");
						if (pDialog.isShowing())
							pDialog.dismiss();
						finish();
					} else {
						AppUtils.toast(Post.this,
								"Error has occured :(\nPlease try again.");
					}
				} catch (JSONException e) {
					Log.w(TAG, "Malformed data received!");
					e.printStackTrace();
				}
			}
			if (pDialog.isShowing())
				pDialog.dismiss();
		}
	}

}
