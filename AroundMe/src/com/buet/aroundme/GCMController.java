package com.buet.aroundme;

import java.util.Random;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.PowerManager;
import android.util.Log;

import com.buet.aroundme.model.ServerResponse;
import com.buet.aroundme.parser.JsonParser;
import com.buet.aroundme.util.AppConstants;
import com.buet.aroundme.util.AppUtils;
import com.google.android.gcm.GCMRegistrar;

public class GCMController {
	private static final String TAG = "GCMController";

	private final int MAX_ATTEMPTS = 5;
	private final int BACKOFF_MILLI_SECONDS = 2000;
	private final Random random = new Random();
	private static Context tContext;

	public GCMController(Context context) {
		tContext = context;
	}

	// Register this account with the server.
	// final int id, final String deviceId,
	public void register(final Context context, final String regId) {
		Log.i(TAG, "registering device (regId = " + regId + ")");
		long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);

		// Once GCM returns a registration id, we need to register on our server
		// In case the server is down, we will retry it a couple of times.
		for (int i = 1; i <= MAX_ATTEMPTS; i++) {
			Log.d(TAG, "Attempt #" + i + " to register");

			try {
				Log.i(TAG, "Trying to register in server: " + i + "/"
						+ MAX_ATTEMPTS);
				// Post registration values to our server
				if (postGCMRegIdToServer(AppConstants.REQ_USER_GCM_REGISTER,
						regId))
					GCMRegistrar.setRegisteredOnServer(context, true);
				Log.i(TAG, "GCM+Server registration complete");
				return;
			} catch (Exception e) {
				// Here we are simplifying and retrying on any error; in a real
				// application, it should retry only on unrecoverable errors
				// (like HTTP error code 503).
				Log.e(TAG, "Failed to register on attempt " + i + ":" + e);
				if (i == MAX_ATTEMPTS) {
					break;
				}
				try {
					Log.d(TAG, "Sleeping for " + backoff + " ms before retry");
					Thread.sleep(backoff);
				} catch (InterruptedException e1) {
					// Activity finished before we complete - so just exit.
					Log.d(TAG, "Thread interrupted: abort remaining retries!");
					Thread.currentThread().interrupt();
					return;
				}
				// increase backoff exponentially
				backoff *= 2;
			}
		}
		Log.e(TAG, "Server registration error!");
	}

	private boolean postGCMRegIdToServer(int reqCode, String regId) {
		// TO_DO Inside AsyncTask's doInBackground : NO
		// => MainActivity calls it from a null-able AsyncTask, while
		// GCMIntentService calls it as a service from background.
		// Hence no need for another AysncTask hazel for now :)
		JsonParser jsonParser = new JsonParser();
		JSONObject jObj = new JSONObject();
		try {
			jObj.put("request", reqCode);
			jObj.put("gcm_id", regId);
			jObj.put("imei", AppUtils.getDeviceIMEI(tContext));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String jStr = jObj.toString();
		try {
			ServerResponse response = jsonParser.retrieveServerData(jStr);
			if (response.getStatus() == 200
					&& response.getjObj().getString("data").equals("success")) {
				Log.i("GCMController",
						"GCM data saved to server successfully: regId=" + regId);
				return true;
			}
		} catch (ConnectTimeoutException cte) {
			Log.e(TAG, "Connection to server is lost!\nPlease try again later.");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

	// Unregister this account/device pair within the server.
	public void unregister(final Context context, final String regId) {

		Log.i(TAG, "unregistering device (regId = " + regId + ")");

		try {
			// postGCMRegIdToServer(AppConstants.USER_UNREGISTER_GCM, regId);
			postGCMRegIdToServer(AppConstants.REQ_USER_GCM_UNREGISTER, "");
			GCMRegistrar.setRegisteredOnServer(context, false);
			Log.i(TAG, "Server unregistered");
			// String message = context.getString(R.string.server_unregistered);
			// displayMessageOnScreen(context, message);
		} catch (Exception e) {

			// At this point the device is unregistered from GCM, but still
			// registered in the our server.
			// We could try to unregister again, but it is not necessary:
			// if the server tries to send a message to the device, it will get
			// a "NotRegistered" error message and should unregister the device.

			// String message = context.getString(
			// R.string.server_unregister_error, e.getMessage());
			// displayMessageOnScreen(context, message);
			Log.e(TAG, "Server unregistered error.");
		}
	}

	private PowerManager.WakeLock wakeLock;

	@SuppressWarnings("deprecation")
	@SuppressLint("Wakelock")
	public void acquireWakeLock(Context context) {
		if (wakeLock != null)
			wakeLock.release();

		PowerManager pm = (PowerManager) context
				.getSystemService(Context.POWER_SERVICE);

		wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
				| PowerManager.ACQUIRE_CAUSES_WAKEUP
				| PowerManager.ON_AFTER_RELEASE, "WakeLock");

		wakeLock.acquire();
	}

	public void releaseWakeLock() {
		if (wakeLock != null)
			wakeLock.release();
		wakeLock = null;
	}
}