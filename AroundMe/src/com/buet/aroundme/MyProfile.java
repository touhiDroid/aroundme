/**
 * 
 */
package com.buet.aroundme;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.buet.aroundme.model.ServerResponse;
import com.buet.aroundme.parser.JsonParser;
import com.buet.aroundme.util.AppConstants;
import com.buet.aroundme.util.AppPrefManager;
import com.buet.aroundme.util.AppUtils;
import com.buet.aroundme.util.Base64;

import eu.janmuller.android.simplecropimage.CropImage;

/**
 * @author Touhid
 * 
 */
public class MyProfile extends SherlockActivity implements OnClickListener {

	private final String TAG = "MyProfile";

	private TextView tvName, tvPoints, tvEmail, tvPassword, tvDob, tvContactNo,
			tvAddress;
	private ImageView ivProfilePic;
	private static Context tContext;

	private static final int CAMERA_REQ_CODE = 901;
	private static final int CROP_REQ_CODE = 1001;
	private static final int GALLERY_REQ_CODE = 902;

	private File picFile;
	private Uri mImageCaptureUri;
	private Bitmap scaledBmp;
	private String imgB64Str;
	private ProgressDialog pDialog;

	private String dob;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final ActionBar actionBar = getSupportActionBar();
		actionBar.removeAllTabs();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		setContentView(R.layout.my_profile);

		tContext = MyProfile.this;

		tvName = (TextView) findViewById(R.id.tvNameMyProfile);
		tvPoints = (TextView) findViewById(R.id.tvPointLevelMyProfile);
		tvEmail = (TextView) findViewById(R.id.tvEmailMyProfile);
		tvPassword = (TextView) findViewById(R.id.tvPasswordMyProfile);
		tvDob = (TextView) findViewById(R.id.tvDobMyProfile);
		tvContactNo = (TextView) findViewById(R.id.tvContactNoMyProfile);
		tvAddress = (TextView) findViewById(R.id.tvAddressMyProfile);
		ivProfilePic = (ImageView) findViewById(R.id.ivUserPicMyProfile);
		setViewValues();

		findViewById(R.id.tvNameMyProfile).setOnClickListener(this);
		findViewById(R.id.llEmailMyProfile).setOnClickListener(this);
		findViewById(R.id.llPasswordMyProfile).setOnClickListener(this);
		findViewById(R.id.llDobMyProfile).setOnClickListener(this);
		findViewById(R.id.llContactNoMyProfile).setOnClickListener(this);
		findViewById(R.id.llAddressMyProfile).setOnClickListener(this);
		findViewById(R.id.ivUserPicMyProfile).setOnClickListener(this);
	}

	private void setViewValues() {
		String name = AppPrefManager.getUserName(tContext);
		String email = AppPrefManager.getUserEmail(tContext);
		String dob = AppPrefManager.getUserDob(tContext);
		String contactNo = AppPrefManager.getUserContactNo(tContext);
		String address = AppPrefManager.getUserAddress(tContext);
		String title = "User Profile";
		if (!name.equals(null) && name.length() > 0) {
			tvName.setText(name);
			title = name + "\'s Profile";
		}
		if (!email.equals(null) && email.length() > 0)
			tvEmail.setText(email);
		if (!contactNo.equals(null) && contactNo.length() > 0)
			tvContactNo.setText(contactNo);
		if (!dob.equals(null) && dob.length() > 0)
			tvDob.setText(dob);
		if (!address.equals(null) && address.length() > 0)
			tvAddress.setText(address);
		String level = "Unregistered User", points = "";
		if (AppPrefManager.isUSerProfileComplete(tContext)) {
			level = "General User";
			points = "\nPoints : 0";
		}
		tvPoints.setText(level + points);

		ActionBar ab = getSupportActionBar();
		ab.setTitle(title);
		ab.setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public void onClick(View v) {
		// 1-name, 2-gender, 3-pwd, 4-email, 5-dob, 6-contact, 7-address, 8-pic,
		// 9-points
		switch (v.getId()) {
		case R.id.tvNameMyProfile:
			updateDialog(1, "name", tvName.getText().toString());
			break;
		case R.id.llPasswordMyProfile:
			getCurPwd();
			break;
		case R.id.llEmailMyProfile:
			updateDialog(4, "email", tvEmail.getText().toString());
			break;
		case R.id.llDobMyProfile:
			showDatePickerDialog();
			break;
		case R.id.llContactNoMyProfile:
			updateDialog(6, "contact no", tvEmail.getText().toString());
			break;
		case R.id.llAddressMyProfile:
			updateDialog(7, "address", tvAddress.getText().toString());
			break;
		case R.id.ivUserPicMyProfile:
			showPicTakerDialog();
			break;

		default:
			break;
		}
	}

	private void showDatePickerDialog() {
		Calendar cald = Calendar.getInstance();
		int date = cald.get(Calendar.DATE);
		int mon = cald.get(Calendar.MONTH);
		int year = cald.get(Calendar.YEAR) - 14;
		DatePickerDialog dDialog = new DatePickerDialog(this, dobListnr, year,
				mon, date);
		dDialog.show();
	}

	private OnDateSetListener dobListnr = new OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int mon, int day) {
			int yr = Calendar.getInstance().get(Calendar.YEAR);
			if (year <= yr - 12) {
				dob = day + "-" + (mon + 1) + "-" + year;
				Log.d(TAG, "DoB is set.");
				new UpdateProfileInfoTask(dob).execute(5);
			} else {
				Log.d(TAG, "Sorry! You're too much young to access this app!");
				Toast.makeText(MyProfile.this,
						"Sorry! You're too much young to access this app!",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	private void setFileName() {
		// Set the file name
		String appDir = AppConstants.APP_DIRECTORY.toString();
		String photoFileName = "ame/profile_pic_" + System.currentTimeMillis()
				+ ".jpg";
		Log.d(TAG, photoFileName);
		picFile = new File(appDir, photoFileName);
		if (!picFile.exists()) {
			try {
				if (!AppConstants.APP_DIRECTORY.exists())
					AppConstants.APP_DIRECTORY.mkdirs();
				if (picFile.mkdirs() && picFile.delete()
						&& picFile.createNewFile())
					Log.d(TAG, "PicFile newly created");
			} catch (IOException e) {
				Log.d(TAG, "Exception for new creation of picFile");
				e.printStackTrace();
			}
		} else {
			Log.d(TAG, "picFile exists. Replacing it...");
			if (!picFile.delete())
				Log.d(TAG, "picFile exists. Deletion failed!");
			else
				setFileName();
		}
	}

	private void onClickCameraCapture() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		Log.d(TAG, "cam clicked");
		try {
			setFileName();
			// set uri from the file
			// mImageCaptureUri = Uri.fromFile(picFile);
			if (AppUtils.isSDCardMounted()) {
				mImageCaptureUri = Uri.fromFile(picFile);
			} else {
				Toast.makeText(tContext, "Media Not Mounted!",
						Toast.LENGTH_LONG).show();
				return;
			}
			intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
			intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT,
					mImageCaptureUri);
			intent.putExtra("return-data", true);
			Log.d(TAG, "cam intent starting");
			startActivityForResult(intent, CAMERA_REQ_CODE);
		} catch (ActivityNotFoundException e) {
			Log.d("Error", "Activity Not Found" + e.toString());
		}
	}

	private void startCropImage() {
		Intent intent = new Intent(tContext, CropImage.class);
		intent.putExtra(CropImage.IMAGE_PATH, picFile.getPath());
		intent.putExtra(CropImage.SCALE, true);

		intent.putExtra(CropImage.ASPECT_X, 5);
		intent.putExtra(CropImage.ASPECT_Y, 4);

		Log.d(TAG, "Starting crop from startCropImage()");
		startActivityForResult(intent, CROP_REQ_CODE);
	}

	private void loadPicasaImageFromGallery(final Uri uri) {
		String[] projection = { MediaColumns.DATA, MediaColumns.DISPLAY_NAME };
		Cursor cursor = tContext.getContentResolver().query(uri, projection,
				null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(MediaColumns.DISPLAY_NAME);
			if (columnIndex != -1) {
				new Thread(new Runnable() {
					public void run() {
						try {
							Bitmap bitmap = android.provider.MediaStore.Images.Media
									.getBitmap(tContext.getContentResolver(),
											uri);
							int h = 200 * (bitmap.getHeight() / bitmap
									.getWidth());
							if (h <= 10)
								h = 100;
							scaledBmp = Bitmap.createScaledBitmap(bitmap, 200,
									h, true);

							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									try {
										FileOutputStream out = new FileOutputStream(
												picFile);
										scaledBmp.compress(
												Bitmap.CompressFormat.PNG, 90,
												out);
										mImageCaptureUri = Uri
												.fromFile(picFile);
										out.close();
										// ivProfilePic.setImageBitmap(scaledBmp);
										// btnUpdate.setVisibility(View.VISIBLE);
									} catch (FileNotFoundException e) {
										e.printStackTrace();
									} catch (IOException e) {
										e.printStackTrace();
									}
								}
							});

						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
				}).start();
			}
		}
		cursor.close();

	}

	private void getCurPwd() {
		final Dialog dialog = new Dialog(this,
				android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
		dialog.setContentView(R.layout.update_profile_element);
		TextView tv = (TextView) dialog.findViewById(R.id.tvUpdateHeaderUPE);
		tv.setText("Enter your current password:");
		final EditText et = (EditText) dialog
				.findViewById(R.id.etUpdateFieldUPE);
		et.setHint("Current Password");
		et.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);

		(dialog.findViewById(R.id.btnUpdateUPE))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.cancel();
						if (AppPrefManager.matchPassword(tContext, et.getText()
								.toString()))
							updateDialog(3, "password", "");
						else
							Toast.makeText(tContext, "Password mismatches!",
									Toast.LENGTH_LONG).show();
					}
				});
		(dialog.findViewById(R.id.btnCancelUPE))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.cancel();
					}
				});

		// Center-focus the dialog
		Window window = dialog.getWindow();
		window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);

		// The below code is EXTRA - to dim the parent view by 70% :D
		WindowManager.LayoutParams lp = window.getAttributes();
		lp.dimAmount = 0.7f;
		lp.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		dialog.getWindow().setAttributes(lp);
		dialog.show();
	}

	private void updateDialog(final int uCode, String field, String oldValue) {
		final Dialog dialog = new Dialog(this,
				android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
		dialog.setContentView(R.layout.update_profile_element);
		TextView tv = (TextView) dialog.findViewById(R.id.tvUpdateHeaderUPE);
		tv.setText("Update your " + field + "?");
		final EditText et = (EditText) dialog
				.findViewById(R.id.etUpdateFieldUPE);
		et.setText(oldValue);

		(dialog.findViewById(R.id.btnUpdateUPE))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.cancel();
						String nVal = et.getText().toString();
						Toast.makeText(tContext, "Updating info...",
								Toast.LENGTH_SHORT).show();
						updateViews(uCode, nVal);
						new UpdateProfileInfoTask(nVal).execute(uCode);
					}
				});
		(dialog.findViewById(R.id.btnCancelUPE))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.cancel();
					}
				});

		// Center-focus the dialog
		Window window = dialog.getWindow();
		window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);

		// The below code is EXTRA - to dim the parent view by 70% :D
		WindowManager.LayoutParams lp = window.getAttributes();
		lp.dimAmount = 0.7f;
		lp.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		dialog.getWindow().setAttributes(lp);
		dialog.show();
	}

	protected void updateViews(int uCode, String nVal) {
		// 1-name, 2-gender, 3-pwd, 4-email, 5-dob, 6-contact, 7-address, 8-pic,
		// 9-points
		switch (uCode) {
		case 1:
			tvName.setText(nVal);
			break;
		case 3:
			String s = "";
			for (int i = 0; i < nVal.length(); i++)
				s += "*";
			tvPassword.setText(s);
			break;
		case 4:
			tvEmail.setText(nVal);
			break;
		case 5:
			tvDob.setText(nVal);
			break;
		case 6:
			tvContactNo.setText(nVal);
			break;
		case 7:
			tvAddress.setText(nVal);
			break;
		case 9:
			break;

		default:
			break;
		}
	}

	private void showPicTakerDialog() {
		final Dialog dialog = new Dialog(tContext,
				android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
		dialog.setContentView(R.layout.pic_taker_dialog);
		dialog.findViewById(R.id.btn_camera_pic_taker_dlg).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.cancel();
						onClickCameraCapture();
					}
				});
		dialog.findViewById(R.id.btn_browse_pic_taker_dlg).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.cancel();
						setFileName();
						Intent intent = new Intent(Intent.ACTION_PICK);
						intent.setType("image/*");
						startActivityForResult(intent, GALLERY_REQ_CODE);
					}
				});

		// Center-focus the dialog
		Window window = dialog.getWindow();
		window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);

		// The below code is EXTRA - to dim the parent view by 70% :D
		WindowManager.LayoutParams lp = window.getAttributes();
		lp.dimAmount = 0.7f;
		lp.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		dialog.getWindow().setAttributes(lp);
		dialog.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			switch (requestCode) {
			case CAMERA_REQ_CODE:
				Log.d(TAG, "cam onActivityResult");
				try {
					Bitmap bmp = AppUtils.decodeFileToBitmap(picFile, 500);
					int h = 200 * (bmp.getHeight() / bmp.getWidth());
					if (h <= 10)
						h = 100;
					int angle = AppUtils.getCorrectionAngleForCam(picFile);
					if (angle == 0) {
						scaledBmp = Bitmap
								.createScaledBitmap(bmp, 200, h, true);
					} else {
						Matrix mat = new Matrix();
						mat.postRotate(angle);
						Bitmap correctBmp = Bitmap.createBitmap(bmp, 0, 0,
								bmp.getWidth(), bmp.getHeight(), mat, true);
						scaledBmp = Bitmap.createScaledBitmap(correctBmp, 200,
								h, true);
						Log.d("", "scaled");
					}

					try {
						FileOutputStream out = new FileOutputStream(picFile);
						scaledBmp.compress(Bitmap.CompressFormat.PNG, 90, out);
						mImageCaptureUri = Uri.fromFile(picFile);
						out.close();
					} catch (Exception e) {
						Log.e("Error_Touhid", e.toString());
					}
					// code b4 crop: ivProfilePic.setImageBitmap(scaledBmp);
					// btnUpdate.setVisibility(View.VISIBLE);
				} catch (IOException e) {
					Toast.makeText(tContext, "IOException - Failed to load",
							Toast.LENGTH_SHORT).show();
					Log.e("Camera", e.toString());
				} catch (OutOfMemoryError oom) {
					Toast.makeText(tContext, "OOM error - Failed to load",
							Toast.LENGTH_SHORT).show();
					Log.e("Camera", oom.toString());
				}
				Log.d(TAG, "starting crop");
				startCropImage();
				//
				// doCrop();
				// imageUriToString = mImageCaptureUri.toString();
				break;

			case GALLERY_REQ_CODE:
				try {
					mImageCaptureUri = data.getData();

					if (AppUtils.getPathFromURI(tContext, mImageCaptureUri) != null) {
						try {
							InputStream inputStream = tContext
									.getContentResolver().openInputStream(
											mImageCaptureUri);
							FileOutputStream fileOutputStream = new FileOutputStream(
									picFile);
							AppUtils.copyI2OStream(inputStream,
									fileOutputStream);
							fileOutputStream.close();
							inputStream.close();
						} catch (FileNotFoundException e) {
							System.out.println("Picasa Image,"
									+ "coz FileNotFoundException!");
							loadPicasaImageFromGallery(mImageCaptureUri);
						}
					} else {
						System.out.println("Picasa Image!");
						loadPicasaImageFromGallery(mImageCaptureUri);
					}
					Log.d(TAG, "Gal code: starting crop");
					startCropImage();

				} catch (Exception e) {
					Log.e(TAG, "Error while creating the picture file", e);
				}
				break;

			case CROP_REQ_CODE:
				String path = data.getStringExtra(CropImage.IMAGE_PATH);
				Log.d(TAG, "path = " + path);
				if (path == null) {
					return;
				}
				scaledBmp = BitmapFactory.decodeFile(picFile.getPath());
				int h = 200 * (scaledBmp.getHeight() / scaledBmp.getWidth());
				if (h <= 10)
					h = 100;
				scaledBmp = Bitmap.createScaledBitmap(scaledBmp, 200, h, true);
				ivProfilePic.setVisibility(View.VISIBLE);
				ivProfilePic.setImageBitmap(AppUtils.getRoundedImage(scaledBmp,
						200));
				ByteArrayOutputStream bao = new ByteArrayOutputStream();
				scaledBmp.compress(Bitmap.CompressFormat.JPEG, 90, bao);
				byte[] ba = bao.toByteArray();
				imgB64Str = Base64.encodeToString(ba, Base64.DEFAULT);
				break;
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private class UpdateProfileInfoTask extends
			AsyncTask<Integer, Void, JSONObject> {

		private final String TAG = this.getClass().getSimpleName();
		private JsonParser jsonParser;
		private String nVal;
		private int uCode;

		public UpdateProfileInfoTask(String newValue) {
			nVal = newValue;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			jsonParser = new JsonParser();
		}

		@Override
		protected JSONObject doInBackground(Integer... params) {
			Log.d(TAG, "inside doInBackground()");
			uCode = params[0];
			int uid = AppPrefManager.getUserID(tContext);
			String imei = AppUtils.getDeviceIMEI(tContext);
			Log.i(TAG, "Requesting update for uid=" + uid + ", u_code=" + uCode
					+ ", new value=" + nVal);

			JSONObject jObj = new JSONObject();
			try {
				jObj.put("request", AppConstants.REQ_USER_UPDATE_INFO);
				jObj.put("uid", uid);
				jObj.put("imei", imei);
				jObj.put("u_code", uCode);
				jObj.put("new_value", nVal);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			try {
				ServerResponse response = jsonParser.retrieveServerData(jObj
						.toString());
				if (response.getStatus() == 200) { // HTTP_STATUS_CODE
					Log.i(TAG, "success in updating user:" + uid + "::\n"
							+ response.toString());
					JSONObject responseObj = response.getjObj();
					return responseObj;
				} else
					return null;
			} catch (ConnectTimeoutException cte) {
				AppUtils.showConnectionTimeOutToast(MyProfile.this);
			} catch (Exception e) {
				Log.e(TAG, "Exception in retrieveServerData" + e.toString());
			}
			return null;
		}

		@Override
		protected void onPostExecute(JSONObject responseObj) {
			super.onPostExecute(responseObj);
			(((Vibrator) tContext.getSystemService(Context.VIBRATOR_SERVICE)))
					.vibrate(300);
			if (responseObj != null) {
				try {
					String status = responseObj
							.getString(AppConstants.REQ_STATUS);
					Log.d(TAG, "Status received: " + status);
					if (status.equals(AppConstants.REQ_SUCCESS)) {
						// TO_DO Update shared-preference
						updatePrefData();
					}
					Toast.makeText(tContext,
							responseObj.getString(AppConstants.REQ_DATA),
							Toast.LENGTH_LONG).show();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}// end of onPostExecute()

		private void updatePrefData() {
			switch (uCode) {
			case 1:
				break;

			default:
				break;
			}
		}
	}

}
