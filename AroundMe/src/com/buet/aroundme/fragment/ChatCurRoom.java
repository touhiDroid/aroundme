/**
 * 
 */
package com.buet.aroundme.fragment;

import java.util.ArrayList;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;
import com.buet.aroundme.R;
import com.buet.aroundme.adapter.ChatMessageAdapter;
import com.buet.aroundme.model.ChatMessage;
import com.buet.aroundme.model.ServerResponse;
import com.buet.aroundme.parser.JsonParser;
import com.buet.aroundme.util.AppConstants;
import com.buet.aroundme.util.AppPrefManager;
import com.buet.aroundme.util.AppUtils;

/**
 * @author Touhid
 * 
 */
public class ChatCurRoom extends SherlockFragment {

	private final String TAG = this.getClass().getSimpleName();

	private Activity tActivity;
	private ChatMessageAdapter chatAdapter;

	private ArrayList<ChatMessage> chatList;
	private EditText etMsg;
	private Location location;

	public static SherlockFragment newInstance() {
		return new ChatCurRoom();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		tActivity = activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView");
		ViewGroup rv = (ViewGroup) inflater.inflate(R.layout.chat_cur,
				container, false);

		ListView lvNews = (ListView) rv.findViewById(R.id.lvChat);
		chatList = new ArrayList<>();
		// TODO Initialize list
		chatAdapter = new ChatMessageAdapter(getActivity(),
				R.layout.chat_other_row, chatList);
		etMsg = (EditText) rv.findViewById(R.id.etChatter);
		location = AppUtils.getLocation(tActivity);
		rv.findViewById(R.id.btnSendChat).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						new CMPostTask().execute();
					}
				});
		lvNews.setAdapter(chatAdapter);
		if (AppUtils.isInternetAvailable(tActivity))
			new GetChatMessages().execute(-1);

		return rv;
	}

	private class GetChatMessages extends AsyncTask<Integer, Void, JSONObject> {

		private JsonParser jsonParser;
		private ProgressDialog pDialog;
		private int lid;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (jsonParser == null)
				jsonParser = new JsonParser();
			showProgressDialog();
		}

		private void showProgressDialog() {
			if (pDialog == null)
				pDialog = new ProgressDialog(tActivity);
			if (!pDialog.isShowing()) {
				pDialog.setMessage("Posting your message ...");
				pDialog.setCancelable(false);
				pDialog.setIndeterminate(true);
				pDialog.show();
			}
		}

		@Override
		protected JSONObject doInBackground(Integer... params) {
			lid = params[0];

			JSONObject jObj = new JSONObject();
			try {
				jObj.put("longi", location.getLongitude());
				jObj.put("latti", location.getLatitude());
				if (lid < 0)
					jObj.put("request", AppConstants.REQ_NEWS_GET_LAT_10_LL);
				else {
					jObj.put("request", AppConstants.REQ_NEWS_GET_NXT_10_LL);
					jObj.put("lid", lid);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			ServerResponse response;
			try {
				response = jsonParser.retrieveServerData(jObj.toString());
			} catch (ConnectTimeoutException e) {
				e.printStackTrace();
				AppUtils.toast(tActivity, "Connection is timed out!");
				return null;
			}
			if (response.getStatus() == 200) {
				Log.d(">>>><<<<", "success in retrieving news.");
				JSONObject responseObj = response.getjObj();
				return responseObj;
			} else
				return null;
		}

		@Override
		protected void onPostExecute(JSONObject responseObj) {
			super.onPostExecute(responseObj);
			if (pDialog.isShowing())
				pDialog.dismiss();
			if (responseObj != null) {
				try {
					String status = responseObj.getString(
							AppConstants.REQ_STATUS).trim();
					Log.d(TAG, "Status = " + status);
					if (status.equals(AppConstants.REQ_SUCCESS)) {
						// newsAdapter.setData();
						// newsAdapter.notifyDataSetChanged();
						processChatList(ChatMessage.parseList(responseObj
								.getJSONArray(AppConstants.REQ_DATA)));
					} else {
						AppUtils.toast(tActivity,
								"Error has occured in refreshing news-list :(\nPlease try again later.");
					}
				} catch (JSONException e) {
					Log.w(TAG, "Malformed data received!");
					AppUtils.toast(tActivity, "Malformed data received!");
					e.printStackTrace();
				}
			}
		}

		private void processChatList(ArrayList<ChatMessage> parsedList) {
			Log.i("???????", "news count = " + parsedList.size());
			// Decide on adding new data to the list
			if (parsedList.size() == 0) {
				AppUtils.toast(tActivity, "You've caught our all news!");
				// noMoreNews = true;
			} else if (lid < 0) {
				chatList = parsedList;
				chatAdapter.setData(chatList);
			} else {
				chatList.addAll(parsedList);
				chatAdapter.addData(parsedList);
			}
			chatAdapter.notifyDataSetChanged();
			if (parsedList.size() < 10)
				// noMoreNews = true;
				if (parsedList.size() > 0) {
					// int l = parsedList.get(parsedList.size() - 1).getId();
					// if (l < lastNewsId)
					// lastNewsId = l;
					// for (News n : parsedList)
					// newsDb.insertNews(n);
				}
		}
	}

	private class CMPostTask extends AsyncTask<Void, Void, JSONObject> {

		private JsonParser jsonParser;
		private ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (jsonParser == null)
				jsonParser = new JsonParser();
			showProgressDialog();
		}

		private void showProgressDialog() {
			if (pDialog == null)
				pDialog = new ProgressDialog(tActivity);
			if (!pDialog.isShowing()) {
				pDialog.setMessage("Posting your message ...");
				pDialog.setCancelable(false);
				pDialog.setIndeterminate(true);
				pDialog.show();
			}
		}

		@Override
		protected JSONObject doInBackground(Void... params) {
			JSONObject jObj = new JSONObject();
			try {
				jObj.put("request", AppConstants.REQ_CHAT_POST);
				jObj.put("uid", AppPrefManager.getUserID(tActivity));
				jObj.put("body", etMsg.getText().toString());
				jObj.put("longi", location.getLongitude());
				jObj.put("latti", location.getLatitude());
				// jObj.put("lat", );
			} catch (JSONException e) {
				e.printStackTrace();
			}

			ServerResponse response;
			try {
				response = jsonParser.retrieveServerData(jObj.toString());
			} catch (ConnectTimeoutException e) {
				e.printStackTrace();
				// AppUtils.toast(tContext, "Connection is timed out!");
				return null;
			}
			if (response.getStatus() == 200) {
				Log.d(">>>><<<<", "success in retrieving notifications.");
				JSONObject responseObj = response.getjObj();
				return responseObj;
			} else
				return null;
		}

		@Override
		protected void onPostExecute(JSONObject responseObj) {
			super.onPostExecute(responseObj);
			if (pDialog.isShowing())
				pDialog.dismiss();
			if (responseObj != null) {
				try {
					String status = responseObj
							.getString(AppConstants.REQ_STATUS);
					if (status.equals(AppConstants.REQ_SUCCESS)) {
						Log.d(TAG, "Status is ok");
						AppUtils.toast(tActivity, "Your news is posted :)");
						int id = 0, uid = 0, seen_count = 0;
						double longi = 0, latti = 0;
						String updated_at = "", created_at = "", body = "", img_url = "";
						ChatMessage cm = new ChatMessage(id, uid, seen_count,
								longi, latti, updated_at, created_at, body,
								img_url);
						chatList.add(cm);
						chatAdapter.add(cm);
						chatAdapter.notifyDataSetChanged();
						etMsg.setText("");
					} else {
						AppUtils.toast(tActivity,
								"Error has occured :(\nPlease try again.");
					}
				} catch (JSONException e) {
					Log.w(TAG, "Malformed data received!");
					e.printStackTrace();
				}
			}
		}
	}
}
