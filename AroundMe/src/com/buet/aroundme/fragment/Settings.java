/**
 * 
 */
package com.buet.aroundme.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.buet.aroundme.R;

/**
 * @author Touhid
 * 
 */
public class Settings extends SherlockFragment {

	public static SherlockFragment newInstance() {
		return new Settings();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		final ActionBar actionBar = getSherlockActivity().getSupportActionBar();
		actionBar.removeAllTabs();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		return inflater.inflate(R.layout.settings, container, false);
	}
}
