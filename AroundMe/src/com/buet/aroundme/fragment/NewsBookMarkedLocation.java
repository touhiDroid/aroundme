/**
 * 
 */
package com.buet.aroundme.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.actionbarsherlock.app.SherlockFragment;
import com.buet.aroundme.R;

/**
 * @author Touhid
 * 
 */
public class NewsBookMarkedLocation extends SherlockFragment {

	private final String TAG = this.getClass().getSimpleName();

	// private ArrayList<NewsItem> newsList;
	private static View rv;

	// private NewsAdapter adapter;

	public static SherlockFragment newInstance() {
		return new NewsBookMarkedLocation();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView");
		rv = inflater.inflate(R.layout.news_cur, container, false);

		// rv.setBackgroundResource(R.color.Orange);

		// ListView lvNews = (ListView) rv.findViewById(R.id.lvNews);
		// newsList = new ArrayList<>();
		// setDummyNewsList();
		// adapter = new NewsAdapter(getActivity(), R.layout.news_item,
		// newsList);
		//
		// ScaleInAnimationAdapter mAnimAdapter = new ScaleInAnimationAdapter(
		// adapter);
		// mAnimAdapter.setAbsListView(lvNews);
		// lvNews.setAdapter(mAnimAdapter);
		// lvNews.setAdapter(adapter);
		// setRetainInstance(true);

		return rv;
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		Log.e(TAG, "setUserVisibleHint : " + isVisibleToUser);
	}

	// private void setDummyNewsList() {
	// String nb =
	// "News body News body News body News body News body News body";
	// String t = "7pm, 15 Oct, 2014";
	// for (int i = 0; i < 15; i++)
	;
	// newsList.add(new NewsItem(1, 12, 2, 3, 1, 2, 1, 1, t, t,
	// "Headline 1", nb, ""));
	// }

}
