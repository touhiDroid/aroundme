/**
 * 
 */
package com.buet.aroundme.fragment;

import java.util.ArrayList;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;
import com.buet.aroundme.NewsDetails;
import com.buet.aroundme.R;
import com.buet.aroundme.adapter.NewsAdapter;
import com.buet.aroundme.model.NewsItem;
import com.buet.aroundme.model.ServerResponse;
import com.buet.aroundme.parser.JsonParser;
import com.buet.aroundme.util.AppConstants;
import com.buet.aroundme.util.AppUtils;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

/**
 * @author Touhid
 * 
 */
public class NewsCurLocation extends SherlockFragment {

	private final String TAG = this.getClass().getSimpleName();
	private Activity tActivity;
	private Location location;

	private ArrayList<NewsItem> newsList;
	private NewsAdapter newsAdapter;
	private PullToRefreshListView mPullRefreshListView;

	private ProgressDialog pDialog;
	private boolean noMoreNews = false;
	private int lastNewsId = -1;

	public static SherlockFragment newInstance() {
		return new NewsCurLocation();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		tActivity = activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView");
		ViewGroup rv = (ViewGroup) inflater.inflate(R.layout.news_cur,
				container, false);

		getSherlockActivity().getSupportActionBar().setSubtitle(
				"News of Your Location");
		newsList = new ArrayList<>();
		setupPullableListView(rv);

		location = AppUtils.getLocation(tActivity);
		lastNewsId = -1;
		if (AppUtils.isInternetAvailable(tActivity))
			new GetNewsTask().execute(lastNewsId);

		return rv;
	}

	private void setupPullableListView(ViewGroup rv) {
		mPullRefreshListView = (PullToRefreshListView) rv
				.findViewById(R.id.ptrlvNews);
		// Set a listener to be invoked when the list should be refreshed.
		mPullRefreshListView.setOnRefreshListener(newsRefresher());
		ListView actualListView = mPullRefreshListView.getRefreshableView();
		// Need to use the Actual ListView when registering for Context Menu
		registerForContextMenu(actualListView);
		newsAdapter = new NewsAdapter(tActivity, newsList);
		// addSoundListener();
		// You can also just use setListAdapter(mAdapter) or
		// mPullRefreshListView.setAdapter(mAdapter)
		actualListView.setAdapter(newsAdapter);
		actualListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent i = new Intent(tActivity, NewsDetails.class);
				i.putExtra("news",
						(NewsItem) parent.getItemAtPosition(position));
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(i);
			}
		});
	}

	private OnRefreshListener<ListView> newsRefresher() {
		return new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(final PullToRefreshBase<ListView> refreshView) {
				((Vibrator) tActivity
						.getSystemService(Context.VIBRATOR_SERVICE))
						.vibrate(100);
				String label = DateUtils.formatDateTime(tActivity,
						System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME
								| DateUtils.FORMAT_SHOW_DATE
								| DateUtils.FORMAT_ABBREV_ALL);
				// Update the LastUpdatedLabel
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								refreshView.onRefreshComplete();
							}
						});
					}
				}, 2000);
				// TO_DO work to refresh the list here.
				if (noMoreNews)
					AppUtils.alert(tActivity,
							"You've caught all of our news!\nNo more news remains.");
				else if (AppUtils.isInternetAvailable(tActivity))
					new GetNewsTask().execute(lastNewsId);// ;
				else
					AppUtils.showNoInternetAlert(tActivity);
			}
		};
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		Log.e(TAG, "setUserVisibleHint : " + isVisibleToUser);
		// if (newsList != null)
		// setDummyNewsList();
		// else
		// Log.e(TAG, "News List is null!!!");
	}

	// private void setDummyNewsList() {
	// String nb =
	// "News body News body News body News body News body News body";
	// String t = "7pm, 15 Oct, 2014";
	// for (int i = 1; i < 16; i++)
	// newsList.add(new NewsItem(1, 12, 2, 3, 1, 2, 1, 1, t, t,
	// "Headline " + i, nb + i, ""));
	// }

	private void showProgressDialog() {
		if (pDialog == null)
			pDialog = new ProgressDialog(tActivity);
		if (!pDialog.isShowing()) {
			pDialog.setMessage("Getting news for your current location ...");
			pDialog.setCancelable(false);
			pDialog.setIndeterminate(true);
			pDialog.show();
		}
	}

	private class GetNewsTask extends AsyncTask<Integer, Void, JSONObject> {

		private JsonParser jsonParser;
		private int lid;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (jsonParser == null)
				jsonParser = new JsonParser();
			showProgressDialog();
		}

		@Override
		protected JSONObject doInBackground(Integer... params) {
			lid = params[0];

			JSONObject jObj = new JSONObject();
			try {
				jObj.put("longi", location.getLongitude());
				jObj.put("latti", location.getLatitude());
				if (lid < 0)
					jObj.put("request", AppConstants.REQ_NEWS_GET_LAT_10_LL);
				else {
					jObj.put("request", AppConstants.REQ_NEWS_GET_NXT_10_LL);
					jObj.put("lid", lid);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			ServerResponse response;
			try {
				response = jsonParser.retrieveServerData(jObj.toString());
			} catch (ConnectTimeoutException e) {
				e.printStackTrace();
				AppUtils.toast(tActivity, "Connection is timed out!");
				return null;
			}
			if (response.getStatus() == 200) {
				Log.d(">>>><<<<", "success in retrieving news.");
				JSONObject responseObj = response.getjObj();
				return responseObj;
			} else
				return null;
		}

		@Override
		protected void onPostExecute(JSONObject responseObj) {
			super.onPostExecute(responseObj);
			if (pDialog.isShowing())
				pDialog.dismiss();
			if (responseObj != null) {
				try {
					String status = responseObj.getString(
							AppConstants.REQ_STATUS).trim();
					Log.d(TAG, "Status = " + status);
					if (status.equals(AppConstants.REQ_SUCCESS)) {
						// newsAdapter.setData();
						// newsAdapter.notifyDataSetChanged();
						processNewsList(NewsItem.parseList(responseObj
								.getJSONArray(AppConstants.REQ_DATA)));
					} else {
						AppUtils.toast(tActivity,
								"Error has occured in refreshing news-list :(\nPlease try again later.");
					}
				} catch (JSONException e) {
					Log.w(TAG, "Malformed data received!");
					AppUtils.toast(tActivity,"Malformed data received!");
					e.printStackTrace();
				}
			}
		}

		private void processNewsList(ArrayList<NewsItem> parsedList) {
			Log.i("???????", "news count = " + parsedList.size());
			// Decide on adding new data to the list
			if (parsedList.size() == 0) {
				AppUtils.toast(tActivity, "You've caught our all news!");
				noMoreNews = true;
			} else if (lid < 0) {
				newsList = parsedList;
				newsAdapter.setData(newsList);
			} else {
				newsList.addAll(parsedList);
				newsAdapter.addData(parsedList);
			}
			newsAdapter.notifyDataSetChanged();
			if (parsedList.size() < 10)
				noMoreNews = true;
			if (parsedList.size() > 0) {
				int l = parsedList.get(parsedList.size() - 1).getId();
				if (l < lastNewsId)
					lastNewsId = l;
				// for (News n : parsedList)
				// newsDb.insertNews(n);
			}
		}
	}
}

// ListView lvNews = (ListView) rv.findViewById(R.id.lvNews);
// setDummyNewsList();
// newsAdapter = new NewsAdapter(getSherlockActivity(),
// R.layout.news_item, newsList);

// ScaleInAnimationAdapter mAnimAdapter = new ScaleInAnimationAdapter(
// newsAdapter);
// mAnimAdapter.setAbsListView(lvNews);
// lvNews.setAdapter(mAnimAdapter);
// lvNews.setAdapter(newsAdapter);
// lvNews.setOnItemClickListener(new OnItemClickListener() {
//
// @Override
// public void onItemClick(AdapterView<?> parent, View view,
// int position, long id) {
// Intent i = new Intent(getSherlockActivity(), NewsDetails.class);
// i.putExtra("news",
// (NewsItem) parent.getItemAtPosition(position));
// i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
// startActivity(i);
// }
// });
// setRetainInstance(true);
