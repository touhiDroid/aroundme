/**
 * 
 */
package com.buet.aroundme.fragment;

import java.io.Serializable;
import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.ActionBar.TabListener;
import com.actionbarsherlock.app.SherlockFragment;
import com.buet.aroundme.R;
import com.buet.aroundme.util.DepthPageTransformer;

/**
 * @author Touhid
 * 
 */
public class News extends SherlockFragment implements TabListener {

	private final String TAG = this.getClass().getSimpleName();
	private ViewPager vPager;
	private ViewPagerAdapter vpAdapter;
	private ArrayList<SherlockFragment> newsFragList;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setRetainInstance(true);
		Log.i(TAG, "onCreate : bundle-data null? : "
				+ (savedInstanceState == null));
	}

	public static SherlockFragment newInstance() {
		return new News();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView");
//		setRetainInstance(true);
//		View v = setupViewNewly(inflater, container);
		View rv = inflater.inflate(R.layout.news_pager, container, false);
		vPager = (ViewPager) rv.findViewById(R.id.pager);
		vpAdapter = new ViewPagerAdapter(getSherlockActivity()
				.getSupportFragmentManager());
		vPager.setAdapter(vpAdapter);
		vPager.setPageTransformer(true, new DepthPageTransformer());
		
		final ActionBar actionBar = getSherlockActivity().getSupportActionBar();
		actionBar.removeAllTabs();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.addTab(actionBar.newTab().setText("Current Location")
				.setTabListener(this));
		actionBar.addTab(actionBar.newTab().setText("Bookmarked Location")
				.setTabListener(this));
		vPager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int pos) {
				actionBar.setSelectedNavigationItem(pos);
			}
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
		Log.d(TAG, "bundle null - so initializing");
		newsFragList = new ArrayList<>();
		newsFragList.add(NewsCurLocation.newInstance());
		newsFragList.add(NewsBookMarkedLocation.newInstance());

		return rv;
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		Log.d(TAG, "News Fragment : setUserVisibleHint");
	}

	@Override
	public void onResume() {
		super.onResume();
		vPager.setCurrentItem(0);
	}

//	private View setupViewNewly(LayoutInflater inflater, ViewGroup container) {
//		
//	}

	// @Override
	// public void onSaveInstanceState(Bundle outState) {
	// // TO_DO Auto-generated method stub
	// super.onSaveInstanceState(outState);
	// outState.putSerializable("news_frag_list", newsFragList);
	// // outState.putSerializable("pager", (Serializable) vPager);
	// // outState.putSerializable("adapter", (Serializable) vpAdapter);
	// }

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		Log.d(TAG, "Selecting tab : " + tab.getText());
		vPager.setCurrentItem(tab.getPosition(), true);
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		Log.d(TAG, "Unselecting tab : " + tab.getText());
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		Log.d(TAG, "Reselecting tab : " + tab.getText());
	}

	private class ViewPagerAdapter extends FragmentPagerAdapter implements
			Serializable {
		private static final long serialVersionUID = 844224165013627189L;
		private int NUM_PAGES = 2;

		public ViewPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			Log.i(TAG, "News getting item: " + position);
			return newsFragList.get(position);
		}

		@Override
		public int getCount() {
			return NUM_PAGES;
		}
	}

}
