/**
 * 
 */
package com.buet.aroundme.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.ActionBar.TabListener;
import com.actionbarsherlock.app.SherlockFragment;
import com.buet.aroundme.R;

/**
 * @author Touhid
 * 
 */
public class Chat extends SherlockFragment implements TabListener {

	private final String TAG = this.getClass().getSimpleName();
	private ViewPager vPager;
	
	public static SherlockFragment newInstance() {
		return new Chat();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView");
		View rv = inflater.inflate(R.layout.chat_pager, container, false);
		vPager = (ViewPager) rv.findViewById(R.id.pager);
		vPager.setAdapter(new ViewPagerAdapter(getSherlockActivity()
				.getSupportFragmentManager()));

		final ActionBar actionBar = getSherlockActivity().getSupportActionBar();
		actionBar.removeAllTabs();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.addTab(actionBar.newTab().setText("Current Room")
				.setTabListener(this));
		actionBar.addTab(actionBar.newTab().setText("Bookmarked Rooms")
				.setTabListener(this));
		vPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int pos) {
				actionBar.setSelectedNavigationItem(pos);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});

		return rv;
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		Log.d(TAG, "Slecting tab : " + tab.getText());
		vPager.setCurrentItem(tab.getPosition(), true);
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		Log.d(TAG, "Unslecting tab : " + tab.getText());
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		Log.d(TAG, "Reslecting tab : " + tab.getText());
	}

	private class ViewPagerAdapter extends FragmentPagerAdapter {

		private int NUM_PAGES = 2;

		public ViewPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			switch (position) {
			case 0:
				return new ChatCurRoom();
			case 1:
				return new ChatBookmarkedRoom();

			default:
				return null;
			}
		}

		@Override
		public int getCount() {
			return NUM_PAGES;
		}

	}

}
