/**
 * 
 */
package com.buet.aroundme.interfaces;

/**
 * @author Touhid
 *
 */
public interface RetryCallbackForNoInternet {
	
	public void retry(int retrialSerial);
}
