package com.buet.aroundme;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.buet.aroundme.util.AppConstants;
import com.buet.aroundme.util.AppUtils;
import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {

	private static final String TAG = "GCMIntentService";

	private static GCMController gcmController = null;

	public GCMIntentService() {
		// Call extended class Constructor GCMBaseIntentService
		super(AppConstants.GOOGLE_PROJECT_ID);
	}

	/**
	 * Method called on device registered
	 **/
	@Override
	protected void onRegistered(Context context, String registrationId) {
		Log.i(TAG, "Device registered: regId = " + registrationId);
		// Get Global Controller Class object (see application tag in
		// AndroidManifest.xml)
		if (gcmController == null)
			gcmController = new GCMController(context);

		Log.i(TAG, "Your device has just registred with GCM: " + registrationId);
		gcmController.register(context, registrationId);
	}

	/**
	 * Method called on device unregistered
	 * */
	@Override
	protected void onUnregistered(Context context, String registrationId) {
		if (gcmController == null)
			gcmController = new GCMController(context);
		Log.i(TAG, "Device unregistered: regId=" + registrationId);
		gcmController.unregister(context, registrationId);
	}

	/**
	 * Method called on Receiving a new message from GCM server
	 * */
	@Override
	protected void onMessage(Context context, Intent intent) {
		if (gcmController == null)
			gcmController = new GCMController(context);

		Log.i(TAG, "Received message");
		String message = intent.getExtras().getString("message");
		int targetId = intent.getExtras().getInt("target_id");
		int type = intent.getExtras().getInt("type");
		// notifies user
		AppUtils.generateNotification(context, message, type, targetId);
	}

	/**
	 * Method called on receiving a deleted message
	 * */
	@Override
	protected void onDeletedMessages(Context context, int total) {
		if (gcmController == null)
			gcmController = new GCMController(context);
		Log.i(TAG, "Received deleted messages notification");
		// notifies user
		AppUtils.generateNotification(context,
				"Received deleted messages notification", -1, -1);
	}

	/**
	 * Method called on Error
	 * */
	@Override
	public void onError(Context context, String errorId) {
		if (gcmController == null)
			gcmController = new GCMController(context);
		Log.i(TAG, "Received error: " + errorId);
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		if (gcmController == null)
			gcmController = new GCMController(context);
		Log.i(TAG, "Received recoverable error: " + errorId);
		return super.onRecoverableError(context, errorId);
	}

}
