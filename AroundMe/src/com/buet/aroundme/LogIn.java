/**
 * 
 */
package com.buet.aroundme;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.buet.aroundme.model.ServerResponse;
import com.buet.aroundme.parser.JsonParser;
import com.buet.aroundme.util.AppConstants;
import com.buet.aroundme.util.AppPrefManager;
import com.buet.aroundme.util.AppUtils;

/**
 * @author Touhid
 * 
 */
public class LogIn extends SherlockActivity {

	// private final int SPLASH_TIME_OUT = 2500;
	private static final String TAG = "LogIn";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.log_in);
		AppUtils.getLocation(this);

		final EditText etp = (EditText) findViewById(R.id.etPasswordLogIn);
		findViewById(R.id.btnLogIn).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				etp.getText().toString();
			}
		});

	}

	private class LogInTask extends AsyncTask<String, Void, JSONObject> {

		private final String TAG = this.getClass().getSimpleName();
		private JsonParser jsonParser;
		private Context tContext;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			jsonParser = new JsonParser();
		}

		@Override
		protected JSONObject doInBackground(String... params) {
			Log.d(TAG, "inside doInBackground()");
			String password = params[0];
			String imei = AppUtils.getDeviceIMEI(tContext);

			JSONObject jObj = new JSONObject();
			try {
				jObj.put("request", AppConstants.REQ_USER_UPDATE_INFO);
				jObj.put("imei", imei);
				jObj.put("password", password);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			try {
				ServerResponse response = jsonParser.retrieveServerData(jObj
						.toString());
				if (response.getStatus() == 200) { // HTTP_STATUS_CODE
					Log.i(TAG, "success in logging in" + response.toString());
					JSONObject responseObj = response.getjObj();
					return responseObj;
				} else
					return null;
			} catch (ConnectTimeoutException cte) {
				AppUtils.showConnectionTimeOutToast(LogIn.this);
			} catch (Exception e) {
				Log.e(TAG, "Exception in retrieveServerData" + e.toString());
			}
			return null;
		}

		@Override
		protected void onPostExecute(JSONObject responseObj) {
			super.onPostExecute(responseObj);
			(((Vibrator) tContext.getSystemService(Context.VIBRATOR_SERVICE)))
					.vibrate(300);
			if (responseObj != null) {
				try {
					String status = responseObj
							.getString(AppConstants.REQ_STATUS);
					Log.d(TAG, "Status received: " + status);
					if (status.equals(AppConstants.REQ_SUCCESS)) {
						// TO_DO Update shared-preference
					}
					Toast.makeText(tContext,
							responseObj.getString(AppConstants.REQ_DATA),
							Toast.LENGTH_LONG).show();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}// end of onPostExecute()

	}
}
